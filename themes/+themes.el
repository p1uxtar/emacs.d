;;; +themes.el --- Summary -*- lexical-binding: t -*-

;; Author: p1uxtar
;; Maintainer: p1uxtar
;; Version: v1.0
;; Package-Requires: (custom themes Hesperus/Phosphorus)

;;; Commentary:

;; Switch (custom) themes.

;;; Code:

;; -------- switch between dark/light themes --------
(defun themes-switch-Hesperus ()
  "Switch themes to Hesperus."
  (interactive)
  (progn
    (disable-theme 'Phosphorus)
    (load-theme 'Hesperus t))
  ;; (when (display-graphic-p)
  ;;   (awesome-tray-mode 1))
  (when (eq system-type 'windows-nt)
    (p1uxtar-Win-frame-size)))

(defun themes-switch-Phosphorus ()
  "Switch themes to Phosphorus."
  (interactive)
  (progn
    (disable-theme 'Hesperus)
    (load-theme 'Phosphorus t))
  ;; (when (display-graphic-p)
  ;;   (awesome-tray-mode 1))
  (when (eq system-type 'windows-nt)
    (p1uxtar-Win-frame-size)))

(defun themes-switch-doom-dark ()
  "Switch to doom dark theme."
  (interactive)
  (progn
    (disable-theme 'doom-one-light)
    (load-theme 'doom-one t))
  (custom-set-faces
   '(font-lock-comment-face ((t (:slant italic))))
   '(font-lock-constant-face ((t (:weight semi-bold))))
   '(font-lock-keyword-face ((t (:weight semi-bold :slant italic))))
   '(org-table ((t :family "Source Code Pro"))))
  (when (featurep 'pyim)
    (setq curchg-default-cursor-color "gold"
          curchg-input-method-cursor-color "#e67e22")))
(global-set-key (kbd "C-c H") #'themes-switch-doom-dark)

(defun themes-switch-doom-light ()
  "Switch to doom light theme."
  (interactive)
  (progn
    (disable-theme 'doom-one)
    (load-theme 'doom-one-light t))
  (custom-set-faces
   '(font-lock-comment-face ((t (:slant italic))))
   '(font-lock-constant-face ((t (:weight semi-bold))))
   '(font-lock-doc-face ((t (:background "#efeae9")))) ;; #fdefca
   '(font-lock-keyword-face ((t (:weight semi-bold :slant italic))))
   '(font-lock-string-face ((t (:background "#ecf3ec")))) ;; #fdefca
   '(highlight ((t (:background "#c4e3ff" :foreground "#3D535A"))))
   ;; '(ivy-current-match ((t (:extend t :background "#b8b8b8"))))
   ;; '(ivy-posframe ((t (:background "#d4d4d4"))))
   '(org-table ((t :family "Source Code Pro"))))
  (when (featurep 'pyim)
    (setq curchg-default-cursor-color "#4078f2"
          curchg-input-method-cursor-color "#8d4bbb")))
(global-set-key (kbd "C-c P") #'themes-switch-doom-light)

(defun themes-switch-kaolin-dark ()
  "Switch to kaolin dark theme."
  (interactive)
  (progn
    (disable-theme 'kaolin-valley-light)
    (load-theme 'kaolin-valley-dark t))
  (custom-set-faces
   '(cursor ((t (:background "gold"))))
   '(font-lock-comment-face ((t (:slant italic))))
   '(font-lock-constant-face ((t (:weight semi-bold))))
   '(font-lock-keyword-face ((t (:weight semi-bold :slant italic))))
   '(hl-line ((t (:extend t :background "#101420"))))
   '(ivy-current-match ((t (:background "#101420"))))
   '(org-table ((t :family "Source Code Pro"))))
  (when (featurep 'pyim)
    (setq pyim-indicator-cursor-color (list "#e67e22" "gold"))))
(global-set-key (kbd "C-c H") #'themes-switch-kaolin-dark)

(defun themes-switch-kaolin-light ()
  "Switch to kaolin light theme."
  (interactive)
  (progn
    (disable-theme 'kaolin-valley-dark)
    (load-theme 'kaolin-valley-light t))
  (custom-set-faces
   '(cursor ((t (:background "#4078f2"))))
   '(font-lock-comment-face ((t (:slant italic))))
   '(font-lock-constant-face ((t (:weight semi-bold))))
   ;; '(font-lock-doc-face ((t (:background "#efeae9")))) ;; #fdefca
   '(font-lock-keyword-face ((t (:weight semi-bold :slant italic))))
   '(font-lock-string-face ((t (:background "#F3DEBB")))) ;; #fdefca
   '(highlight ((t (:background "#EFB7FF" :foreground "#3D535A"))))
   ;; '(ivy-cursor ((t (:background "#F0C99A" :foreground "#5E5854"))))
   '(ivy-current-match ((t (:extend t :background "#F0C99A"))))
   '(ivy-posframe ((t (:background "#F3E7D3"))))
   '(org-table ((t :family "Source Code Pro"))))
  (when (featurep 'pyim)
    (setq pyim-indicator-cursor-color (list "#8d4bbb" "#4078f2"))))
(global-set-key (kbd "C-c P") #'themes-switch-kaolin-light)

;; (when (eq system-type 'windows-nt)
;;   (p1uxtar-Win-frame-size))

;; --------- switch themes --------
(cond
 ((eq system-type 'gnu/linux)
  (themes-switch-kaolin-dark))
 ((eq system-type 'windows-nt)
  (themes-switch-kaolin-light)))

;; path of (custom) themes
;; (add-to-list 'load-path (expand-file-name "themes" user-emacs-directory))

(provide '+themes)
;;; +themes.el ends here
