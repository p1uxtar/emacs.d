;;; init.el --- Emacs configuration -*- lexical-binding: t -*-

;; Author: Yuanchen Xie
;; Package-Requires: ("GNU/Emacs")
;; Homepage: gitee.com/con5tella/emacs.d
;; Keywords: Emacs, .emacs.d

;;; Commentary:

;; This project is my Emacs custom configurations,
;; Not part of GNU Emacs.

;;; Code:

;; ;; -------- config path --------
(dolist (config-site
         '("config"
           "site-lisp"))
  (add-to-list 'load-path
               (expand-file-name
                config-site user-emacs-directory)))

(dolist (configs
         '(+base
           +package
           +submodule
           +editing
           +color-rg
           +completion
           +lsp-bridge
           +window-frame
           +mode-line
           +modus-themes
           +font
           +cursor
           +program
           +highlight-display
           +cc                ;; c/c++
           +hanh              ;; 漢語(hanh-ngyoq)輸入與翻譯
           +markdown
           +tex               ;; LaTeX
           ;; +org-mode
           +python
           +ESS-R             ;; Emacs Speaks Statistics, R
           +ESS-SAS           ;; Emacs Speaks Statistics, SAS
           +shell             ;; shell
           +function          ;; custom functions
           +comment           ;; separedit
           +vcs               ;; version control system
           +msWin             ;; for MS Windows
           ))
  (require configs))

;; -------- custom-set-variables & custom-set-faces --------
(let ((custom-file (expand-file-name "custom.el" user-emacs-directory)))
  (when (file-exists-p custom-file)
    (load custom-file 'no-error 'no-message)))

;;; init.el ends here
