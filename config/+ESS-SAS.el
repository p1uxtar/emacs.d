;;; +ESS-SAS.el --- config for SAS -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- update ESS (for SAS on Win) --------
(use-package ess-site
  :ensure ess
  :defer t
  :if (eq system-type 'windows-nt)
  :init
  (font-lock-add-keywords 'SAS-mode '(("create\\|from\\|having\\|group by\\|order by\\|when" . 'font-lock-keyword-face)))
  (defun p1uxtar-SAS-newline ()
    (interactive)
    (progn
      (search-forward ";")
      (newline-and-indent)))
  (add-hook 'SAS-mode-hook
            (lambda ()
              (local-set-key (kbd "C-e") 'mwim-end-of-code-or-line)
              (local-set-key (kbd "C-c C-p") 'beginning-of-sas-proc)
              (local-set-key (kbd "C-c C-n") 'next-sas-proc)
              (local-set-key (kbd "M-<return>") 'p1uxtar-SAS-newline)
              (local-set-key (kbd "C-<return>") 'p1uxtar-SAS-newline)
              (local-set-key (kbd "<f7>") 'ess-sas-goto-lst)
              (local-set-key (kbd "<f8>") 'ess-sas-submit)))
  :mode
  ("\\.log" . SAS-log-mode)
  ("\\.lst" . ess-listing-minor-mode)
  :config
  (defun symbol-overlay-ignore-function-sas (symbol)
    "Determine whether SYMBOL should be ignored (SAS Language)."
    (symbol-overlay-match-keyword-list
     symbol
     '("array" "by" "compare" "compress" "contents" "create" "data"
       "delete" "do" "drop" "else" "end" "find" "format" "from" "group"
       "having" "if" "in" "index" "join" "keep" "label" "left" "length"
       "libname" "max" "mean" "means" "merge" "min" "missing" "on"
       "options" "order" "out" "proc" "put" "quit" "random" "rename"
       "retain" "return" "right" "run" "select" "set" "sort" "sql" "sum"
       "summary" "table" "tables" "then" "union" "var" "value" "when"
       "where")))
  (with-eval-after-load 'symbol-overlay
    (add-to-list 'symbol-overlay-ignore-functions '(SAS-mode . symbol-overlay-ignore-function-sas)))
  (let* ((SASpath "/SASHome/SASFoundation/9.4/sas.exe"))
    (if (string= (system-name) "RZ-ZZ-PC-PG004")
        (setq-default ess-sas-submit-command (concat "d:" SASpath))
      (setq-default ess-sas-submit-command (concat "C:/Progra~1" SASpath)))))

;; ;; custom switch & highlight functions
;; (defun p1uxtar-highlight-sas-log ()
;;   (interactive)
;;   (highlight-regexp "^ERROR.*:" font-lock-warning-face)
;;   (highlight-regexp "^NOTE:" font-lock-variable-name-face))
;; (add-hook 'SAS-log-mode-hook #'p1uxtar-highlight-sas-log)

(provide '+ESS-SAS)

;;; +ESS-SAS.el ends here
