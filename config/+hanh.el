;;; +hanh.el --- hanh-ngyoq input & translate -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- English-Chinese Bing dictionary --------
(use-package bing-dict
  :bind
  (("C-c t" . 'bing-dict-brief)))

;; -------- auto switch OS input method --------
(use-package sis
  :hook
  ((markdown-mode org-mode)
   . (lambda ()
       ;; enable the /follow context/ mode
       (sis-context-mode t)
       ;; enable the /inline english/ mode
       ;; 中文输入法状态下，中文后<spc>自动切换英文，结束后自动切回中文
       (sis-inline-mode t)))
  :init
  ;; `C-s/r' 默认优先使用英文，必须在 sis-global-respect-mode 前配置
  (setq sis-respect-go-english-triggers
        ;; isearch-forward 命令时默认进入
        (list 'isearch-forward 'isearch-backward)
        sis-respect-restore-triggers
        ;; isearch-forward 恢复, isearch-exit `<Enter>', isearch-abor `C-g'
        (list 'isearch-exit 'isearch-abort))
  (setq sis-prefix-override-keys (list "C-c" "C-x" "C-h" "<f1>" "<f10>" "C->"))
  (setq sis-default-cursor-color "purple2" ;; 英文光标色
        sis-other-cursor-color "DarkCyan") ;; 中文光标色
  ;; (setq
  ;;  ;; 删除头部空格，默认1，删除一个空格，1/0/'all
  ;;  sis-inline-tighten-head-rule 'zero
  ;;  ;; 删除尾部空格，默认1，删除一个空格，1/0/'all
  ;;  sis-inline-tighten-tail-rule 'zero
  ;;  ;; 默认是t, 中文context下输入<spc>进入内联英文
  ;;  sis-inline-with-english t
  ;;  ;; 默认是nil，而且prog-mode不建议开启, 英文context下输入<spc><spc>进行内联中文
  ;;  sis-inline-with-other t)
  :config
  (cond
   ((eq system-type 'gnu/linux)
    (cond
     ((executable-find "fcitx5")
      (sis-ism-lazyman-config "1" "2" 'fcitx5))
     ((executable-find "fcitx")
      (sis-ism-lazyman-config "1" "2" 'fcitx))))
   ((eq system-type 'windows-nt)
    (sis-ism-lazyman-config nil t 'w32)))
  (sis-global-cursor-color-mode t)
  ;; enable the /respect/ mode buffer 输入法状态记忆模式
  (sis-global-respect-mode t))

(provide '+hanh)

;;; +hanh.el ends here
