;;; +cc.el --- c & c++ -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- update c & c++ --------

;; just-in-time compile via: sunlin7
(defun con5-compile-project-or-file ()
  "Compile current file just smart."
  (interactive)
  (cond
   ((and (boundp 'ede-minor-mode) ede-minor-mode (ede-current-project))
    (ede-compile-target))
   ((file-readable-p "Makefile")
    (compile compile-command))
   ((file-readable-p "makefile")
    (compile compile-command))
   ((string= "c-mode" major-mode)
    (let ((default-directory temporary-file-directory))
      (shell-command-on-region (point-min) (point-max)
                               "gcc -g -O0 -x c -std=gnu11 -o a - && ./a")))
   ((string= "c++-mode" major-mode)
    (let ((default-directory temporary-file-directory))
      (shell-command-on-region (point-min) (point-max)
                               "g++ -g -O0 -x c++ -std=c++11 -o a - && ./a")))
   ((call-interactively 'compile))))

(dolist (cc-hook (list
                  'c-mode-hook
                  'c++-mode-hook))
  (add-hook cc-hook (lambda ()
                      (local-set-key (kbd "<f5>") #'con5-compile-project-or-file))))

(provide '+cc)

;;; +cc.el ends here
