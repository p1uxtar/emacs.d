;;; +ESS-R.el --- config for R -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- ESS smart operators (for R) --------
(use-package ess-smart-equals
  :defer t
  :init
  (setq ess-smart-equals-extra-ops '(brace paren percent))
  (dolist (ess-r-relative-mode-hook
           (list
            'ess-r-mode-hook
            'inferior-ess-r-mode-hook
            'ess-r-transcript-mode-hook
            'ess-roxy-mode-hook))
    (add-hook ess-r-relative-mode-hook
              (lambda ()
                (ess-smart-equals-mode))))
  :config
  (ess-smart-equals-activate))

;; package by shuguang79(ShuguangSun)
(use-package ess-view-data
  :defer t
  :init
  (setq ess-view-data-current-backend 'data.table+magrittr
        ess-view-data-current-update-print-backend 'kable)
  :bind
  ;; TODO ess-vied-data-select
  (("C-c C-v" . 'ess-view-data-print)
   (:map ess-view-data-mode-map
         ("q" . 'kill-current-buffer))))

;; insert variable (column) names or values in ESS-R
(use-package ess-r-insert-obj
  :bind
  (("C-," . 'hydra-ess-r-insert-obj/body))
  :config
  (defhydra hydra-ess-r-insert-obj ()
    ("d" ess-r-insert-obj-dt-name "數據表名稱" :column "data.frame")
    ("c" ess-r-insert-obj-col-name "變量名" :column "column")
    ("C" ess-r-insert-obj-col-name-all "所有變量")
    ("v" ess-r-insert-obj-value "變量值" :column "value")
    ("V" ess-r-insert-obj-value-all "所有變量值")))

;; -------- update ESS (for R on Linux) --------
(use-package ess-site
  :ensure ess
  :defer t
  :init
  (setq comint-prompt-read-only t
        comint-scroll-to-bottom-on-input 'this
        comint-scroll-to-bottom-on-output 'others
        ess-ask-for-ess-directory nil
        ess-history-file "~/R/.R_history"
        ;; ess-indent-level 2
        ;; ess-set-style 'GNU
        ;; ess-set-style 'RStudio-
        tab-always-indent t
        ess-use-eldoc t
        ess-use-flymake nil
        inferior-R-args "--no-save --quiet")

  ;; -------- highlight function names & keywords in R --------
  (dolist (hook '(ess-mode-hook ess-r-mode-hook inferior-ess-r-mode-hook))
    (add-hook hook
              (lambda()
                (font-lock-add-keywords
                 nil
                 '(("\\<\\(if\\|for\\|function\\|return\\|$\\|@\\)\\>[\n[:blank:]]*(" 1
                    font-lock-keyword-face) ; must go first to override highlighting below
                   ("\\<\\([.A-Za-z][._A-Za-z0-9]*\\)[\n[:blank:]]*(" 1
                    font-lock-function-name-face) ; highlight function names
                   ("\\([(,]\\|[\n[:blank:]]*\\)\\([.A-Za-z][._A-Za-z0-9]*\\)[\n[:blank:]]*=[^=]"
                    2 font-lock-builtin-face)
                   ;; highlight numbers
                   ("\\(-?[0-9]*\\.?[0-9]*[eE]?-?[0-9]+[iL]?\\)" 1 font-lock-constant-face)
                   ;; highlight operators
                   ("\\(\\$\\|\\@\\|\\!\\|\\%\\|\\^\\|\\&\\|\\*\\|\(\\|\)\\|\{\\|\}\\|\\[\\|\\]\\|\\-\\|\\+\\|\=\\|\\/\\|\<\\|\>\\|:\\)" 1 font-lock-type-face)
                   ;; highlight S4 methods
                   ("\\(setMethod\\|setGeneric\\|setGroupGeneric\\|setClass\\|setRefClass\\|setReplaceMethod\\)" 1 font-lock-preprocessor-face)
                   ;; highlight packages called through ::, :::
                   ("\\(\\w+\\):\\{2,3\\}" 1 font-lock-type-face)
                   )))))

  ;; (autoload 'ess-r-mode "ess-site.el" nil t)
  (eval-after-load "comint"
    '(progn
       (define-key comint-mode-map "\C-p"
                   'comint-previous-matching-input-from-input)
       (define-key comint-mode-map "\C-n"
                   'comint-next-matching-input-from-input)
       (define-key comint-mode-map "\C-u"
                   'comint-kill-input)))

  (let ((r-home "D:/repos/rhome"))
    (when (and (eq system-type 'windows-nt)
               (executable-find "R")
               (file-exists-p r-home))
      (setq ess-startup-directory r-home)))

  :commands R
  :bind
  (("C-c r" . #'run-ess-r-newest)))

;; -------- poly-mode --------
(use-package polymode
  :defer t
  :mode (("\\.Rmd\\'" . Rmd-mode))
  :init
  (defun Rmd-mode ()
    "ESS Markdown mode for Rmd files"
    (interactive)
    (require 'poly-R)
    (require 'poly-markdown)
    (R-mode)
    (poly-markdown+r-mode)))

(provide '+ESS-R)

;;; +ESS-R.el ends here
