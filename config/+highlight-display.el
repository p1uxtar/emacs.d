;;; +highlight-display.el --- highlight keywords or display more pretty -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- highlight numeric literals in source code --------
(use-package highlight-numbers
  :defer t
  :hook
  (prog-mode . highlight-numbers-mode))

;; -------- highlight brackets delimiters such as (), [] or {} --------
(use-package rainbow-delimiters
  :defer t
  :diminish rainbow-delimiters-mode
  :hook
  ((prog-mode . rainbow-delimiters-mode)
   (rainbow-delimiters-mode . show-paren-mode)))

;; -------- colorizing strings of color names --------
(use-package rainbow-mode
  :defer t
  :diminish rainbow-mode
  :hook
  ((emacs-lisp-mode web-mode css-mode ess-r-mode) . rainbow-mode))

(provide '+highlight-display)

;;; +highlight-display.el ends here
