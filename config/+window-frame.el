;;; +window-frame.el --- config -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- split window right/below --------
;; via emacs-china.org/t/topic/945/2
(defun con5-split-window-right ()
  "Split window with another buffer."
  (interactive)
  (select-window (split-window-right))
  (switch-to-buffer (other-buffer)))
(global-set-key (kbd "C-x 3") #'con5-split-window-right)

(defun con5-split-window-below ()
  "Split window with another buffer."
  (interactive)
  (select-window (split-window-below))
  (switch-to-buffer (other-buffer)))
(global-set-key (kbd "C-x 2") #'con5-split-window-below)

;; -------- set frame size --------
(defun con5-cut-feet-fit-framesize ()
  "Frame size be maximum (mainly for Win)."
  (interactive)
  (set-frame-size (selected-frame)
                  ;; (- (x-display-pixel-width) 18)
                  (x-display-pixel-width)
                  (- (x-display-pixel-height) 40)
                  t)
  (set-frame-position (selected-frame) 0 0))

(defun con5-medium-frame-size ()
  "Frame size be medium."
  (interactive)
  (set-frame-size (selected-frame)
                  (round (* (x-display-pixel-width) 0.5))
                  (round (* (x-display-pixel-height) 0.6))
                  t)
  (set-frame-position (selected-frame)
                      (round (* (x-display-pixel-width) 0.4))
                      (round (* (x-display-pixel-height) 0.3))))

;; 啓動時frame尺寸選大
(add-hook 'window-setup-hook
          (lambda ()
            (defvar con5-frame-size-medium nil
              "Set frame size, the default is maximum.")
            (if (eq system-type 'windows-nt)
                (con5-cut-feet-fit-framesize)
              (toggle-frame-maximized))))

;; ;; frame大中變換
;; ;; 受`toggle-frame-maximized'影響，暫不啓用
;; (defun con5-distort-frame-size ()
;;   "Toggle different frame size."
;;   (interactive)
;;   (if (eq con5-frame-size-medium t)
;;       (progn
;;         (con5-cut-feet-fit-framesize)
;;         (setq con5-frame-size-medium nil))
;;     (progn
;;       (con5-medium-frame-size)
;;       (setq con5-frame-size-medium t))))

;; -------- ace-window --------
(use-package ace-window
  :preface
  (defun con5-toggle-window-split ()
    "Toggle windows layout from up/down to left/right, vice versa."
    (interactive)
    (if (= (count-windows) 2)
        (let* ((this-win-buffer (window-buffer))
               (next-win-buffer (window-buffer (next-window)))
               (this-win-edges (window-edges (selected-window)))
               (next-win-edges (window-edges (next-window)))
               (this-win-2nd (not (and (<= (car this-win-edges)
                                           (car next-win-edges))
                                       (<= (cadr this-win-edges)
                                           (cadr next-win-edges)))))
               (splitter
                (if (= (car this-win-edges)
                       (car (window-edges (next-window))))
                    'split-window-horizontally
                  'split-window-vertically)))
          (delete-other-windows)
          (let ((first-win (selected-window)))
            (funcall splitter)
            (if this-win-2nd (other-window 1))
            (set-window-buffer (selected-window) this-win-buffer)
            (set-window-buffer (next-window) next-win-buffer)
            (select-window first-win)
            (if this-win-2nd (other-window 1))))))
  ;; :init
  ;; ;; set default jump keys, workman layout
  ;; (setq aw-keys '(97 115 104 116 103 121 110 101 111 105))
  :bind
  (("C-o" . 'ace-window)
   ("<f8>" . 'hydra-ace-window/body))
  :config
  (defhydra hydra-ace-window ()
    ("[" shrink-window-horizontally "窄" :column "當前窗口變")
    ("]" enlarge-window-horizontally "寬")
    ("{" shrink-window "短")
    ("}" enlarge-window "長")
    ("=" balance-windows "均分" :column "窗口操作")
    ("-" split-window-vertically "橫斬")
    ("/" split-window-horizontally "豎劈")
    ("r" con5-toggle-window-split "轉置" :exit t)
    ("," previous-buffer "前翻頁" :column "buffer")
    ("." next-buffer "後翻頁")
    ("s" ace-swap-window "上下左右互換")
    ("d" ace-delete-window "關閉")
    ("D" kill-buffer-and-window "關閉並關窗")
    ("o" other-window "光標跳轉" :column "其他")
    ("<f8>" toggle-frame-maximized "frame大小變化" :exit t)
    ("C-g" nil "退出")
    ("q" nil "退出"))
  ;; :custom-face
  ;; (aw-leading-char-face ((t (:inherit font-lock-keyword-face :bold t :height 2.0))))
  ;; (aw-mode-line-face ((t (:inherit mode-line-emphasis :bold t))))
  :custom
  (aw-ignore-current t))

(provide '+window-frame)

;;; +window-frame.el ends here
