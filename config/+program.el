;;; +pragram.el --- configuration -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- update prog-mode config --------

(add-hook 'prog-mode-hook
	      (lambda ()
            ;; automatically inserts the corresponding closing parenthesis,
            ;; (when typing open parenthesis) and vice versa.
            (electric-pair-mode +1)
            ;; (prettify-symbols-mode +1) ;; display some symbols more pretty.
            ;; Use large font for function name when programming.
            (face-remap-add-relative 'font-lock-function-name-face :height 150)))

;; --------  keeps code always indented --------
(use-package aggressive-indent
  :defer t
  :diminish aggressive-indent-mode
  :hook
  (prog-mode . aggressive-indent-mode)
  :config
  (dolist (hook (list
                 'artist-mode-hook
                 'python-mode-hook
                 'separedit-mode-hook
                 ))
    (add-hook hook (lambda ()
                     (aggressive-indent-mode -1)))))

;; -------- A universal syntax checker --------
(use-package flymake
  :diminish flymake-mode
  :bind
  ("C-c f" . flymake-show-buffer-diagnostics)
  :hook
  (prog-mode . flymake-mode)
  :init
  (setq flymake-no-changes-timeout nil
        flymake-fringe-indicator-position 'left-fringe)
  :config
  ;; Check elisp with `load-path'
  (defun my-elisp-flymake-byte-compile (fn &rest args)
    "Wrapper for `elisp-flymake-byte-compile'."
    (let ((elisp-flymake-byte-compile-load-path
           (append elisp-flymake-byte-compile-load-path load-path)))
      (apply fn args)))
  (advice-add 'elisp-flymake-byte-compile :around #'my-elisp-flymake-byte-compile))

;; Show flymake errors with sideline
(use-package sideline-flymake
  :diminish sideline-mode
  :custom-face
  (sideline-flymake-error ((t (:height 0.9 :italic t))))
  (sideline-flymake-warning ((t (:height 0.9 :italic t))))
  (sideline-flymake-success ((t (:height 0.9 :italic t))))
  :hook
  (flymake-mode . sideline-mode)
  :init
  (setq sideline-flymake-display-mode 'point
        sideline-backends-right '(sideline-flymake)))

;; -------- symbol-overlay --------
(use-package symbol-overlay
  :defer t
  :diminish symbol-overlay-mode
  :hook
  (prog-mode . symbol-overlay-mode)
  :config
  (defhydra hydra-symbol-overlay ()
    ("i" symbol-overlay-put "標/否" :column "標記")
    ("D" symbol-overlay-remove-all "移除所有標記")
    ("n" symbol-overlay-jump-next "下一個" :column "跳轉")
    ("p" symbol-overlay-jump-prev "上一個")
    ("r" symbol-overlay-rename "重命名" :column "操作"))
  :bind
  (("M-i" . 'hydra-symbol-overlay/body)
   ("M-n" . 'symbol-overlay-jump-next)
   ("M-p" . 'symbol-overlay-jump-prev)))

(provide '+program)

;;; +pragram.el ends here
