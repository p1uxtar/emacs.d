;;; +python.el --- Python configs -*- lexical-binding: t -*-

;;; Commentary:

;; Custom configurations for Python.

;;; Code:

;; -------- Python basic --------
(setq python-indent 4
      python-indent-guess-indent-offset-verbose nil
      python-indent-offset 4
      python-shell-buffer-name "iPython"
	  python-shell-interpreter "ipython"
	  python-shell-interpreter-args "--simple-prompt -i"
      ;; python-shell-prompt-input-regexps "In \\[[0-9]+\\]: "
      ;; python-shell-prompt-output-regexps "Out\\[[0-9]+\\]: "
      ;; python-shell-completion-setup-code "from IPython.core.completerlib import module_completion"
      ;; python-shell-completion-module-string-code "';'.join(module_completion('''%s'''))\n"
      ;; python-shell-completion-string-code "';'.join(get_ipython().Completer.all_completions('''%s'''))\n"
      )

(defun p1uxtar-run-python ()
  (interactive)
  (or (python-shell-get-process) (call-interactively 'run-python))
  (if (region-active-p)
      (python-shell-send-region (region-beginning) (region-end) t)
    (python-shell-send-buffer t)))

(add-hook 'python-mode-hook
          (lambda ()
            (require 'pyvenv) ;; then use `pyvenv-activate' to specify python
            ;; (aggressive-indent-mode -1)
            ;; (electric-operator-mode) ;; add spacing around operators
	        (local-set-key (kbd "C-c C-c") 'p1uxtar-run-python)))

(provide '+python)

;;; +python.el ends here
