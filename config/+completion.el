;;; +completion.el --- complete contents, codes & commands -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- VERTical Interactive COmpletion --------
(use-package vertico
  :bind
  (:map vertico-map
        ("RET" . vertico-directory-enter)
        ("DEL" . vertico-directory-delete-char)
        ("M-DEL" . vertico-directory-delete-word))
  :hook
  ((after-init . vertico-mode)
   (rfn-eshadow-update-overlay . vertico-directory-tidy)))

;; Completion style for matching regexps in any order
(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion))))
  (orderless-component-separator #'orderless-escapable-split-on-space)
  :config
  (orderless-define-completion-style orderless+initialism
    (orderless-matching-styles '(orderless-initialism
                                 orderless-literal
                                 orderless-regexp)))
  (setq completion-category-overrides
        '((command (styles orderless+initialism))
          (symbol (styles orderless+initialism))
          (variable (styles orderless+initialism)))))

;; Enrich existing commands with completion annotations
(use-package marginalia
  :hook
  (after-init . marginalia-mode))

;; Consulting completing-read
(use-package consult
  :hook
  (completion-list-mode . consult-preview-at-point-mode)
  :bind
  (([remap Info-search]     . consult-info)
   ([remap isearch-forward] . consult-line)     ;; ("C-s")
   ("M-y"                   . consult-yank-pop)
   ("M-g d"                 . consult-find) ;; search for files in DIR
   ("M-g e"                 . consult-compile-error)
   ("M-g f"                 . consult-flymake)
   ("M-g r"                 . consult-ripgrep)))

;; A consulting-read interface for yasnippet
(use-package consult-yasnippet
  :bind ("M-g y" . consult-yasnippet))

;; -------- Emacs Mini-Buffer Actions Rooted in Keymaps --------
(use-package embark
  :bind
  (("C-." . embark-act)  ;; pick some comfortable binding
   ("C-;" . embark-dwim) ;; good alternative: M-.
   ([remap describe-bindings] . embark-bindings)
   :map minibuffer-local-map
   ("M-." . my-embark-preview))
  :init
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  ;; Manual preview for non-Consult commands using Embark
  (defun my-embark-preview ()
    "Previews candidate in vertico buffer, unless it's a consult command."
    (interactive)
    (unless (bound-and-true-p consult--preview-function)
      (save-selected-window
        (let ((embark-quit-after-action nil))
          (embark-dwim)))))
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

;; Consult integration for Embark
(use-package embark-consult
  :bind
  (:map minibuffer-mode-map
        ("C-c C-o" . embark-export))
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(provide '+completion)

;;; +completion.el ends here
