;;; +tex.el --- LaTeX -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; LaTeX displayed as WYSIWYG style
(use-package magic-latex-buffer
  :defer t
  :init
  (setq magic-latex-enable-block-highlight nil
	    magic-latex-enable-suscript        t
	    magic-latex-enable-pretty-symbols  t
	    magic-latex-enable-block-align     nil
	    magic-latex-enable-inline-image    nil
	    magic-latex-enable-minibuffer-echo nil))

(defun p1uxtar-TeX-remove-macro ()
  "Remove current \\macro{text} (unwrap) at point."
  (interactive)
  (when (TeX-current-macro)
    (let ((bounds (TeX-find-macro-boundaries))
	      (brace  (save-excursion
		            (goto-char (1- (TeX-find-macro-end)))
		            (TeX-find-opening-brace))))
	  (delete-region (1- (cdr bounds)) (cdr bounds))
	  (delete-region (car bounds) (1+ brace)))
    t))

;; -------- improve the performance of TeX --------
(use-package auctex
  :defer t
  :config
  (add-hook 'LaTeX-mode-hook
	        (lambda ()
	          (magic-latex-buffer)
	          (auto-fill-mode -1)
	          (turn-on-reftex)
	          (setq reftex-plug-into-AUCTeX t
		            TeX-engine 'xetex
		            TeX-show-compilation t
		            TeX-clean-confirm nil)
	          ;; (set (make-local-variable 'pangu-spacing-real-insert-separtor) t)
	          (outline-minor-mode) ;; use C-c @ to fold
	          ;; (hide-body) ;; fold when init
	          (local-set-key (kbd "M-S") #'p1uxtar-TeX-remove-macro)
	          (electric-pair-mode 1)
	          (turn-on-cdlatex)))
  :custom
  ;; (TeX-auto-save t)
  ;; (TeX-parse-self t) ;; conflict with magic-latex-buffer
  (TeX-master nil) ;; edit multi-file
  (when (executable-find "Okular")
    (TeX-view-program-selection '((output-pdf "Okular"))
			                    TeX-source-correlate-start-server t))
  ;; (TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view")))
  ;; (TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)
  (cdlatex-command-alist
   '(;; ("blk" "insert block in beamer" "\\begin{block}{}
     ;;   ?
     ;; \\end{block}" cdlatex-position-cursor nil t nil)
     ;;        ("frm" "insert frame in beamer" "\\begin{frame}{}
     ;;   ?
     ;; \\end{frame}" cdlatex-position-cursor nil t nil)
     ("ind" "insert indent (new paragraph in beamer)" "\\setlength{\\parindent}{1.6em}" cdlatex-position-cursor nil t nil)
     ;; ("cod" "insert code" "\\begin{lstlisting}[language=]
     ;;   ?
     ;; \\end{lstlisting}" cdlatex-position-cursor nil t nil)
     ("Cod" "import code in file" "\\lstinputlisting[language=]{?}" cdlatex-position-cursor nil t nil)))
  (cdlatex-paired-parens "$([{"))


;; -------- defun & macrOs for LaTeX-mode --------

;; (defun HA-TeX-insert-frame ()
;;   (interactive)
;;   (latex-insert-block "frame"))

;; (defun HA-TeX-insert-block ()
;;   (interactive)
;;   (latex-insert-block "block"))

;; (fset 'HA-macro-TeX-indent
;;       (lambda (&optional arg)
;; 	"Keyboard macro."
;; 	(interactive "p")
;; 	(kmacro-exec-ring-item
;; 	 '("\\setlength{\\parindent{1.6em" 0 "%d")
;; 	 arg)))

;; (fset 'HA-macro-TeX-figure-args
;;       (lambda (&optional arg)
;; 	"Keyboard macro."
;; 	(interactive "p")
;; 	(kmacro-exec-ring-item
;; 	 (quote ("\\includegraphics[width=\\textwidth]{" 0 "%d"))
;; 	 arg)))

;; (defun HA-TeX-insert-align ()
;;   (interactive)
;;   (latex-insert-block "align"))


;; (fset 'HA-args-TeX-insert-code
;;       (lambda (&optional arg)
;; 	"Keyboard macro."
;; 	(interactive "p")
;; 	(kmacro-exec-ring-item
;; 	 (quote ("[language=]" 0 "%d"))
;; 	 arg)))

;; (defun HA-TeX-insert-code ()
;;   (interactive)
;;   (latex-insert-block "lstlisting")
;;   (HA-args-TeX-insert-code))

;; (fset 'HA-TeX-input-code
;;       (lambda (&optional arg)
;; 	"Keyboard macro."
;; 	(interactive "p")
;; 	(kmacro-exec-ring-item
;; 	 (quote ("\\lstinputlisting[language=]{" 0 "%d"))
;; 	 arg)))


;; keybindings
;; (global-set-key (kbd "C-c o f") #'HA-TeX-insert-frame)
;; (global-set-key (kbd "C-c o b") #'HA-TeX-insert-block)
;; (global-set-key (kbd "C-c o i") #'HA-macro-TeX-indent)
;; (global-set-key (kbd "C-c o g") #'HA-macro-TeX-figure-args)
;; (global-set-key (kbd "C-c o a") #'HA-TeX-insert-align)
;; (global-set-key (kbd "C-c o c") #'HA-TeX-insert-code)
;; (global-set-key (kbd "C-c o C") #'HA-TeX-input-code)

;; (which-key-add-key-based-replacements
;;   "C-c o" "macrOs"
;;   "C-c o f" "+Frame"
;;   "C-c o b" "+Block"
;;   "C-c o i" "+Indent"
;;   "C-c o g" "+fiGure(Graph)"
;;   "C-c o a" "+Align(MultiFormula)"
;;   "C-c o c" "+Code"
;;   "C-c o C" "<Code"
;;   )

(provide '+tex)

;;; +tex.el ends here
