;;; +modus-themes.el --- config -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(use-package modus-themes
  :defer t
  :init
  (require 'modus-themes)
  (use-package solar
    :config
    (setq calendar-latitude 34.7
          calendar-longitude 113.4))
  (use-package circadian
    :after solar
    :config
    (setq circadian-themes '((:sunrise . modus-operandi)
                             (:sunset  . modus-vivendi)))
    (circadian-setup))
  :custom
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs t)
  (modus-themes-mixed-fonts t)
  (modus-themes-variable-pitch-ui t)
  (modus-themes-fringes 'subtle)
  (modus-themes-subtle-line-numbers t)
  (modus-themes-mode-line '(accented borderless))
  (modus-themes-markup '(background bold italic intense))
  (modus-themes-syntax '(alt-syntax green-strings yellow-comments))
  (modus-themes-hl-line '(underline accented))
  (modus-themes-paren-match '(bold underline intense))
  (modus-themes-links '(neutral-underline background italic))
  (modus-themes-prompts '(intense bold italic))
  (modus-themes-completions '((matches . (extrabold underline intense))
                              (selection . (semibold italic text-also))
                              (popup . (extrabold intense))))
  (modus-themes-region '(accented bg-only no-extend))
  (modus-themes-diffs 'desaturated)
  (modus-themes-org-blocks 'gray-background)
  (modus-themes-headings
   '((1 . (variable-pitch 1.5))
     (2 . (1.3))
     (agenda-date . (1.3))
     (agenda-structure . (variable-pitch light 1.8))
     (t . (rainbow 1.1))))
  :config
  ;; 若使用單一主題，則以下加載命令二選一
  ;; (load-theme 'modus-operandi :no-confirm)
  ;; (modus-themes-load-theme 'modus-operandi)
  (add-hook 'highlight-indentation-mode-hook
            (lambda ()
              (face-remap-add-relative 'highlight-indentation-face :underline nil)))
  :bind
  ("C-c T" . #'modus-themes-toggle))

(provide '+modus-themes)

;;; +modus-themes.el ends here
