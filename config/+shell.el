;;; +shell.el --- configuration -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- term simulator & shell script --------
(use-package sh-mode
  :mode (("\\.shrc\\'" . sh-mode)))
(cond
 ((eq system-type 'gnu/linux)
  (use-package multi-term
    :init
    (let* ((zsh (executable-find "zsh")))
      (if (and (file-exists-p zsh)
               (file-exists-p "~/.zshrc"))
          (setq multi-term-program zsh)
        (setq multi-term-program (executable-find "bash"))))
    :custom
    (multi-term-scroll-show-maximum-output 1000)
    :bind
    (("<f12>" . 'con5-open-term)
     :map term-raw-map
     (("<f12>" . 'previous-buffer)
      ("C-o" . 'ace-window)
      ("C-p" . 'term-send-up)
      ("C-n" . 'term-send-down)
      ("M-DEL" . 'term-send-backward-kill-word)))
    :config
    (defun con5-open-term ()
      (interactive)
      (let ((con5-term-name "*Terminal*"))
        (if (get-buffer con5-term-name)
            (pop-to-buffer con5-term-name)
          (multi-term)
          (with-current-buffer "*terminal<1>*"
            (rename-buffer con5-term-name)))))))
 ((eq system-type 'windows-nt)
  (defun cons5tella-open-eshell ()
    (interactive)
    (let ((con5-term-name "*eshell*"))
      (if (get-buffer con5-term-name)
          (pop-to-buffer con5-term-name)
        (eshell))))
  (global-set-key (kbd "<f12>") 'cons5tella-open-eshell)
  (add-hook 'eshell-mode-hook
            (lambda ()
              (define-key eshell-mode-map (kbd "<f12>") 'previous-buffer)
              (define-key eshell-mode-map (kbd "C-o") 'ace-window)
              (define-key eshell-mode-map (kbd "C-p") 'eshell-previous-matching-input-from-input)
              (define-key eshell-mode-map (kbd "C-n") 'eshell-next-matching-input-from-input)
              (define-key eshell-mode-map (kbd "C-u") 'eshell-kill-input)))))

(provide '+shell)

;;; +shell.el ends here
