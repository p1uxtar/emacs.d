;;; +markdown.el --- md -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- markdown --------
(use-package markdown-mode
  :init
  (setq markdown-enable-math t)
  :custom
  ;; if t, hide marker characters `*' etc.
  (markdown-hide-markup nil)
  (markdown-header-scaling t)
  (markdown-indent-function t)
  ;; (markdown-enable-math t)
  (markdown-hide-urls nil)
  :hook
  (markdown-mode . (lambda ()
                     (electric-pair-mode)
                     (valign-mode)))
  :bind
  (:map markdown-mode-map
        ("C-c `" . 'markdown-insert-gfm-code-block)))

(provide '+markdown)

;;; +markdown.el ends here
