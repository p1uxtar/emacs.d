;;; +submodule.el --- config -*- lexical-binding: t -*-

;;; Commentary:

;; When the first Emacs startup or the submodules not exist:
;; $ git submodule update --init --recursive

;;; Code:

(dolist (list (split-string
               (with-temp-buffer
                 (insert-file-contents
                  (expand-file-name ".gitmodules" user-emacs-directory))
                 (keep-lines "path"
                             (point-min)
                             (point-max))
                 (replace-regexp-in-string
                  "path = \\(.*\\)/\\(.*\\)" "\\1/\\2/\\2.el"
                  (buffer-substring
                   (point-min)
                   (point-max))))))
  (unless (file-exists-p (expand-file-name list user-emacs-directory))
    (when (executable-find "git")
      (eshell-command "cd ~/.emacs.d;git submodule update --init --recursive"))))

(provide '+submodule)

;;; +submodule.el ends here
