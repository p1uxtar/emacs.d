;;; +function.el --- configuration -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(defun con5-select-block ()
  "Select the current/next block plus 1 blankline.
If region is active, extend selection downward by block.

URL `http://xahlee.info/emacs/emacs/emacs_select_text_block.html'
Version: 2019-12-26 2021-08-13 2023-11-14"
  (interactive)
  (if (region-active-p)
      (re-search-forward "\n[ \t]*\n[ \t]*\n*" nil :move)
    (progn
      (skip-chars-forward " \n\t")
      (when (re-search-backward "\n[ \t]*\n" nil :move)
        (goto-char (match-end 0)))
      (push-mark (point) t t)
      (re-search-forward "\n[ \t]*\n" nil :move))))

(global-set-key (kbd "M-h") 'con5-select-block)

;; delete empty lines
;; @mayingjun via emacs-cn
(defun con5-delete-empty-line (beg end)
  "Delete empty lines if selected, otherwise the whole buffer."
  (interactive "r")
  (let ((empty-line "^[\t  ]*$"))
    (save-excursion
      (if (region-active-p)
          (flush-lines empty-line beg end)
        (flush-lines empty-line (point-min) (point-max))))))

(defalias 'con5-delete-blank-line 'con5-delete-empty-line)

;; -------- open init config file --------
(defun con5-open-init-file ()
  "Open to edit init.el file."
  (interactive)
  (find-file (expand-file-name "init.el" user-emacs-directory)))

(global-set-key (kbd "C-c i") #'con5-open-init-file)

;; source: http://steve.yegge.googlepages.com/my-dot-emacs-file
(defun con5-rename-current-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))

(defun con5-switch-to-scratch ()
  "Switch to scratch butter."
  (interactive)
  (switch-to-buffer "*scratch*"))

(global-set-key (kbd "<f9>") #'con5-switch-to-scratch)


;; Switch to last buffer.
(defun con5-last-buffer (arg)
  "Switch to last buffer.
Argument ARG if not nil, switching in a new window."
  (interactive "P")
  (cond
   ((minibufferp)
    (keyboard-escape-quit))
   ((not arg)
    (mode-line-other-buffer))
   (t
    (split-window)
    (mode-line-other-buffer))))

(global-set-key (kbd "<escape>") #'con5-last-buffer)

;; -------- revert current buffer as sudo --------
(defun reopen-file-with-sudo ()
  "Open a file with root permission."
  (interactive)
  (find-alternate-file (format "/sudo::%s" (buffer-file-name))))

;; -------- processing before each quit --------
(defun con5-bye-emacs ()
  "Remove custom.el & packages when quit."
  (interactive)
  ;; delete custom.el
  (let ((custom-file (expand-file-name "custom.el" user-emacs-directory)))
    (when (file-exists-p custom-file)
      (eshell-command (concat "rm " custom-file))))
  ;; remove packages Not in the list
  (package-autoremove)
  ;; C-x C-c
  (save-buffers-kill-terminal))

(global-set-key (kbd "C-x C-c") #'con5-bye-emacs)

(provide '+function)

;;; +function.el ends here
