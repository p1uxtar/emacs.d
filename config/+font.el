;;; +font.el --- fonts config -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(defconst con5-font-size 18)

(defun font-installed-p (font-name)
  "Check if font with FONT-NAME is available."
  (find-font (font-spec :name font-name)))

(when (display-graphic-p)
  ;; default (latin letters)
  (cl-loop for font in '("Cascadia Code" "SF Mono" "Source Code Pro"
                         "Fira Code" "Dejavu Sans Mono" "Consolas")
           when (font-installed-p font)
           return (set-face-attribute
                   'default nil
                   :font (font-spec :family font
                                    :weight 'normal
                                    :slant 'normal
                                    :size con5-font-size)))
  ;; serif fonts (variable-pitch)
  (cl-loop for font in '("Source Serif Pro"
                         "DejaVu Serif" "Noto Serif CJK SC"
                         "思源宋体 CN" "Source Han Serif CN")
           when (font-installed-p font)
           return (set-face-attribute
                   'variable-pitch nil
                   :font (font-spec :family font
                                    :weight 'normal
                                    :slant 'normal
                                    :size con5-font-size)))
  ;; emoji
  (dolist (font '("Noto Color Emoji" "OpenSansEmoji"
                  "Segoe UI Emoji" "EmojiOne Color"
                  "Standard Symbols L" "Symbola" "Symbol"))
    (when (font-installed-p font)
      (set-fontset-font t 'emoji ;; 'unicode '(#xF600 . #xF64F)
                        font nil 'append)))
  ;; CJK characters
  ;; '(#x4e00 . #x9fff) can use charset name to instead
  ;; '(#x3000 . #x9FFF) contain CJK-symbols-&-punctuation
  (setq use-default-font-for-symbols nil)
  (cl-loop for font in '("TsangerJinKai03" ;; 倉耳今楷
                         "思源黑体 CN" "Source Han Sans CN"
                         "LXGW WenKai Mono Regular" ;; 霞鶩文楷
                         "微软雅黑 CN" "Microsoft Yahei UI" "Microsoft Yahei"
                         "Noto Sans CJK SC")
           when (font-installed-p font)
           return
           (dolist (charset '(han symbol kana cjk-misc bopomofo))
             (set-fontset-font
              (frame-parameter nil 'font)
              charset
              (font-spec :name font
                         :weight 'normal
                         :slant 'normal
                         :size con5-font-size)
              nil 'append)))
  ;; CJK-unified-ideographs-extension-B
  (dolist (font '("HanaMinB" "SimSun-ExtB"))
    (when (font-installed-p font)
      (set-fontset-font t '(#x20000 . #x2A6DF)
                        font nil 'append))))

(provide '+font)

;;; +font.el ends here
