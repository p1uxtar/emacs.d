;;; +lsp-bridge.el --- language server client -*- lexical-binding: t -*-

;; Package-Requires: (pip3 (epc orjson sexpdata six setuptools paramiko rapidfuzz))
;; Homepage: https://github.com/manateelazycat/lsp-bridge

;;; Commentary:

;; commentary

;;; Code:

(use-package lsp-bridge
  :load-path "lazycat-bucket/lsp-bridge"
  :diminish lsp-bridge-mode
  :if (file-exists-p
       (expand-file-name "lazycat-bucket/lsp-bridge/lsp-bridge.el"
                         user-emacs-directory))
  :config
  (setq lsp-bridge-enable-log nil)
  (global-set-key (kbd "C-c d") 'lsp-bridge-toggle-sdcv-helper)
  (global-lsp-bridge-mode))

;; -------- yasnippet --------
(use-package yasnippet
  :defer t
  :load-path "site-lisp/yasnippet"
  :if (file-exists-p
       (expand-file-name "site-lisp/yasnippet/yasnippet.el"
                         user-emacs-directory))
  :after lsp-bridge
  :diminish yas-minor-mode
  :config
  (use-package yasnippet-snippets
    :defer t
    :after yasnippet)
  (yas-global-mode))

(provide '+lsp-bridge)

;;; +lsp-bridge.el ends here
