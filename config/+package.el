;;; +package.el --- configuration -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; ;; for melpa
;; (require 'package)

(defconst package-mirror-alist
  '((default
     ("gnu"         . "https://elpa.gnu.org/packages/")
     ("melpa"       . "https://melpa.org/packages/")
     ("org"         . "https://orgmode.org/elpa/"))
    ;; (emacs-china
    ;;  ("gnu"          . "https://elpa.emacs-china.org/gnu/")
    ;;  ("melpa"        . "https://elpa.emacs-china.org/melpa/")
    ;;  ("org"          . "https://elpa.emacs-china.org/org/"))
    (GNU_IP
     ("gnu"          . "http://1.15.88.122/gnu/")
     ("melpa"        . "http://1.15.88.122/melpa/")
     ("org"          . "http://1.15.88.122/org/"))
    (tuna
     ("gnu"          . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
     ("nongnu"       . "http://mirrors.tuna.tsinghua.edu.cn/elpa/nongnu/")
     ("melpa"        . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/"))
    (ustc
     ("gnu"          . "https://mirrors.ustc.edu.cn/elpa/gnu/")
     ("melpa"        . "https://mirrors.ustc.edu.cn/elpa/melpa/"))
    ))

(setq package-archives (assoc-default 'tuna package-mirror-alist))

;; list the wanted packages
(setq con5tella-package-list
      '(ace-pinyin ace-window aggressive-indent auctex bing-dict
                   cdlatex circadian consult-yasnippet diff-hl
                   diminish embark embark-consult ess ess-r-insert-obj
                   ess-smart-equals ess-view-data expand-region
                   highlight-numbers hydra lua-mode magic-latex-buffer
                   magit marginalia modus-themes multiple-cursors
                   multi-term mwim orderless polymode poly-markdown
                   poly-R posframe pyvenv rainbow-delimiters
                   rainbow-mode separedit sideline-flymake sis
                   symbol-overlay undo-tree use-package valign vertico
                   which-key yaml-mode yasnippet-snippets))

;; initialize all the packages, prevent initializing twice
(unless (bound-and-true-p package--initialized)
  (setq package-enable-at-startup nil)
  (package-initialize))

;; fetch the list of packages available
(unless package-archive-contents
  (package-refresh-contents))

;; install the missig packages
(dolist (package con5tella-package-list)
  (unless (package-installed-p package)
    (package-install package)))

;; set package-list to `package-selected-packages'
(setq package-selected-packages con5tella-package-list)

;; ;; ensure environment variables inside Emacs on Mac look the same as in the shell
;; (when (memq window-system '(mac ns x))
;;   (exec-path-from-shell-initialize))

;; ;; -------- auto-package-update --------
;; (use-package auto-package-update
;;   :if (not (daemonp))
;;   :custom
;;   (auto-package-update-interval 7) ;; in days
;;   (auto-package-update-prompt-before-update t)
;;   (auto-package-update-delete-old-versions t)
;;   (auto-package-update-hide-results t)
;;   :config
;;   (auto-package-update-maybe))


;; ;; emacs.stackexchange.com/questions/18253/how-does-package-autoremove-decide-which-packages-to-remove
;; (defun package-autoremove ()
;;   "Remove packages that are no more needed.
;;         Packages that are no more needed by other packages in
;;         `package-selected-packages' and their dependencies
;;         will be deleted."
;;   (interactive)
;;   ;; If `package-selected-packages' is nil, it would make no sense to
;;   ;; try to populate it here, because then `package-autoremove' will
;;   ;; do absolutely nothing.
;;   (when (or package-selected-packages
;;             (yes-or-no-p
;; 	     (format-message
;; 	      "`package-selected-packages' is empty! Really remove ALL packages? ")))
;;     (let ((removable (package--removable-packages)))
;;       (if removable
;; 	  (when (y-or-n-p
;; 		 (format "%s packages will be deleted:\n%s, proceed? "
;; 			 (length removable)
;; 			 (mapconcat #'symbol-name removable ", ")))
;;             (mapc (lambda (p)
;;                     (package-delete (cadr (assq p package-alist)) t))
;; 		  removable))
;;         (message "Nothing to autoremove")))))

;; (defun package--removable-packages ()
;;   "Return a list of names of packages no longer needed.
;; These are packages which are neither contained in
;; `package-selected-packages' nor a dependency of one that is."
;;   (let ((needed (cl-loop for p in package-selected-packages
;;                          if (assq p package-alist)
;;                          ;; `p' and its dependencies are needed.
;;                          append (cons p (package--get-deps p)))))
;;     (cl-loop for p in (mapcar #'car package-alist)
;;              unless (memq p needed)
;;              collect p)))

(provide '+package)

;;; +package.el ends here
