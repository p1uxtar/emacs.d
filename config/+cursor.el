;;; +cursor.el --- cursor moving & doing -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; set as setq-default to avoid changing by cursor-chg
(setq-default cursor-type 'box)

;; -------- ace-pinyin --------
(use-package ace-pinyin
  :defer t
  :diminish ace-pinyin-mode
  :hook
  ((after-init) . ace-pinyin-global-mode)
  :init
  ;; set default jump keys
  ;; (setq avy-keys '(113 115 100 102 103 104 106 107 108)) ;; azerty layout
  ;; (setq avy-keys '(97 111 101 117 105 100 104 116 110 115)) ;; dvorak layout
  (setq avy-keys '(97 115 104 116 103 121 110 101 111 105)) ;; workman layout
  ;; (setq avy-keys '(97 115 100 102 103 104 106 107 108))     ;; qwerty layout
  :config
  (setq avy-background t)
  (setq avy-orders-alist
	    '((avy-goto-char . avy-order-closest)
	      (avy-goto-word-0 . avy-order-closest)))
  :bind
  (;; Jump to one char
   ("M-j" . 'avy-goto-char)
   ("C-M-l" . 'avy-goto-line)))

;; -------- multiple-cursors & expand-region --------
(use-package multiple-cursors
  :custom
  (mc/insert-numbers-default 1)
  :bind
  (("C->" . 'hydra-multiple-cursors/body))
  :config
  (defhydra hydra-multiple-cursors (:hint nil)
    "
 Up^^             Down^^           Miscellaneous           % 2(mc/num-cursors) cursor%s(if (> (mc/num-cursors) 1) \"s\" \"\")
------------------------------------------------------------------
 [_p_]   Next     [_n_]   Next     [_l_] Edit lines  [_0_] Insert numbers
 [_P_]   Skip     [_N_]   Skip     [_a_] Mark all    [_A_] Insert letters
 [_U_]   Unmark   [_u_]   Unmark   [_r_] Search
                [_w_]   word     [_W_] All-Words
                [_s_]   symbol   [_S_] All-Symbols
 [Click] Cursor at point       [_q_] Quit        [_C-g_] Quit"
    ("l" mc/edit-lines :exit t)
    ("a" mc/mark-all-like-this :exit t)
    ("n" mc/mark-next-like-this)
    ("N" mc/skip-to-next-like-this)
    ("u" mc/unmark-next-like-this)
    ("w" mc/mark-next-like-this-word)
    ("s" mc/mark-next-like-this-symbol)
    ("p" mc/mark-previous-like-this)
    ("P" mc/skip-to-previous-like-this)
    ("U" mc/unmark-previous-like-this)
    ;; ("M-w" mc/mark-previous-like-this-word)
    ;; ("M-s" mc/mark-previous-like-this-symbol)
    ("r" mc/mark-all-in-region-regexp :exit t)
    ("W" mc/mark-all-words-like-this-in-defun)
    ("S" mc/mark-all-symbols-like-this-in-defun)
    ("0" mc/insert-numbers :exit t)
    ("A" mc/insert-letters :exit t)
    ("<mouse-1>" mc/add-cursor-on-click)
    ;; Help with click recognition in this hydra
    ("<down-mouse-1>" ignore)
    ("<drag-mouse-1>" ignore)
    ("q" nil)
    ("C-g" nil)))
;; :bind
;; ;; further more keybindings using hydra
;; (("C-S-c" . 'mc/edit-lines)
;;  ("C->" . 'mc/mark-next-like-this)
;;  ("C-<" . 'mc/mark-all-words-like-this-in-defun))

;; -------- move where I mean --------
(use-package mwim
  :defer t
  :bind
  (:map prog-mode-map
        ("C-a" . 'mwim-beginning-of-code-or-line)
        ("C-e" . 'mwim-end-of-line-or-code)))

(provide '+cursor)

;;; +cursor.el ends here
