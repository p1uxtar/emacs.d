;;; +mode-line.el --- config -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; Not display minor-mode
;; uncomment when using CLI or custom mode-line
(add-hook 'emacs-startup-hook
          (lambda ()
            (diminish 'abbrev-mode)
            (diminish 'eldoc-mode)
            (diminish 'page-break-lines-mode)
            (diminish 'valign-mode)))

;; -------- vanilla --------
;; via: xuchengpeng etc
(defvar ml-selected-window nil)
(add-hook 'post-command-hook
          (lambda ()
            (setq ml-selected-window (selected-window))))
(add-hook 'buffer-list-update-hook
          (lambda ()
            (force-mode-line-update t)))
(defun dotemacs-buffer-encoding-abbrev ()
  "The line ending convention used in the buffer."
  (let ((buf-coding (format "%s" buffer-file-coding-system)))
    (if (string-match "\\(dos\\|gb2312\\|gbk\\|gb18030\\|unix\\|utf-8\\|mac\\)" buf-coding)
        (if (string-match "\\(gb*\\)" buf-coding)
            (upcase buf-coding)
          (capitalize (match-string 1 buf-coding)))
      buf-coding)))
(setq-default mode-line-format
	          (list
	           mode-line-front-space
	           '(:eval
		         (if (eq ml-selected-window (selected-window))
		             '(:eval (propertize " 😄" ;; "☻"
			    	                     'face 'font-lock-type-face))
		           '(:eval (propertize " 🙂" ;; "☺"
			                           'face 'font-lock-keyword-face))))
	           " ["
	           ;; insert vs overwrite mode, input-method in a tooltip
               '(:eval (propertize (if overwrite-mode "Ovr" "Ins")
			                       'face 'font-lock-keyword-face
			                       'help-echo (concat "Buffer is in "
			    		                              (if overwrite-mode
			    			                              "overwrite"
			    			                            "insert") " mode")))
	           ;; was this buffer modified since the last save?
	           '(:eval (when (buffer-modified-p)
			             (concat "," (propertize "Mod"
			    		                         'face 'font-lock-warning-face
			    		                         'help-echo "Buffer has been modified"))))
	           ;; is this buffer read-only?
	           '(:eval (when buffer-read-only
			             (concat "," (propertize "RO"
			    		                         'face 'font-lock-type-face
			    		                         'help-echo "Buffer is read-only"))))
	           "] "
	           (propertize "%e" 'face 'font-lock-string-face)
	           '(:eval (propertize "%b" 'face 'font-lock-builtin-face
				                   'help-echo (buffer-file-name))) ;; buffer name
	           " ["
	           (propertize "%l" 'face 'font-lock-type-face) ;; line number
	           ", "
	           (propertize "%C" 'face 'font-lock-type-face) ;; column number from 1 (default 0)
	           "] ("
	           (propertize "%p" 'face 'font-lock-constant-face) ;; percentage of buffer
	           " / "
	           (propertize "%I" 'face 'font-lock-constant-face) ;; buffer size
	           ") "
	           '(:eval (dotemacs-buffer-encoding-abbrev)) ;; %z buffer coding-system
	           " "
               mode-line-modes ;; major/minor mode
	           '(:eval `(vc-mode vc-mode)) ;; version control information
	           " "
	           '(:eval mode-line-misc-info) ;; miscellaneous information
	           mode-line-end-spaces
	           ))

(provide '+mode-line)

;;; +mode-line.el ends here
