;;; +editing.el --- update edit process -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- auto-save --------
(use-package auto-save
  :load-path "lazycat-bucket/auto-save"
  :if (file-exists-p
       (expand-file-name "lazycat-bucket/auto-save/auto-save.el"
                         user-emacs-directory))
  :init
  (setq auto-save-idle 1.5)
  :custom
  (auto-save-slient t)
  (auto-save-delete-trailing-whitespace t)
  :config
  (auto-save-enable)
  (setq auto-save-disable-predicates
        '((lambda ()
            (string-suffix-p
             "dict.yaml"
             (concat (file-name-extension (file-name-base (buffer-name)))
                     "." (file-name-extension (buffer-name)))
             t))
          (lambda ()
            (string-suffix-p
             "gpg"
             (file-name-extension (buffer-name))
             t)))))

;; -------- bind & unbind --------

;; remap "C-h"
;; via: Chris
;; (define-key key-translation-map (kbd "C-h") "")
;; (global-set-key (kbd "C-h") 'backward-delete-char-untabify)
;; via: twlz0ne
;; (global-set-key (kbd "C-h") 'delete-backward-char)
;; (global-set-key (kbd "M-h") 'backward-kill-word)
;; (global-set-key (kbd "<f1>") 'help-command)
;; (define-key isearch-mode-map "\C-h" 'isearch-delete-char)

(global-set-key (kbd "C-r") 'replace-string)
(global-set-key (kbd "C-S-r") 'query-replace-regexp)

;; -------- unbind unneeded keys, via: MatthewZMD
(global-set-key (kbd "C-z") nil)
(global-set-key (kbd "M-z") nil)
(global-set-key (kbd "C-x C-z") nil)

;; set DOUBLE `C-@' as `set-rectangular-region-anchor'(github.com/magnars/multiple-cursors.el#special)
;; via: emacs-china.org/t/c-set-mark-command-c-c-multiple-cursors-set-rectangular-region-anchor/17889
(add-hook 'activate-mark-hook
          (lambda ()
            (local-set-key (kbd "C-@") 'set-rectangular-region-anchor)))
(add-hook 'deactivate-mark-hook
          (lambda ()
            (local-unset-key (kbd "C-@"))))

;; -------- awesome-pair --------
(use-package awesome-pair
  :load-path "lazycat-bucket/awesome-pair"
  :if (file-exists-p
       (expand-file-name "lazycat-bucket/awesome-pair/awesome-pair.el"
                         user-emacs-directory))
  :defer t
  :hook
  ((prog-mode conf-mode markdown-mode org-mode yaml-mode) . awesome-pair-mode)
  :bind
  (("(" . 'awesome-pair-open-round)
   ("[" . 'awesome-pair-open-bracket)
   ("{" . 'awesome-pair-open-curly)
   (")" . 'awesome-pair-close-round)
   ("]" . 'awesome-pair-close-bracket)
   ("}" . 'awesome-pair-close-curly)
   ("=" . 'awesome-pair-equal)
   ("%" . 'awesome-pair-match-paren)
   ("\"" . 'awesome-pair-double-quote)
   ("SPC" . 'awesome-pair-space)
   ("C-h" . 'awesome-pair-backward-delete)
   ("C-d" . 'awesome-pair-forward-delete)
   ("C-k" . 'awesome-pair-kill)
   ("M-k" . con5-awesome-pair-copy)
   ("M-\"" . 'awesome-pair-wrap-double-quote)
   ("M-[" . 'awesome-pair-wrap-bracket)
   ("M-{" . 'awesome-pair-wrap-curly)
   ("M-(" . 'awesome-pair-wrap-round)
   ("M-s" . 'awesome-pair-unwrap)
   ("C-M-f" . 'awesome-pair-jump-right)
   ("C-M-b" . 'awesome-pair-jump-left)
   ("C-<return>" . 'awesome-pair-jump-out-pair-and-newline))
  :config
  (defun con5-awesome-pair-copy ()
    "Combine `awesome-pair-kill' & `yank' to instead `thing-copy-to-line-end'."
    (interactive)
    (let ((pt (point)))
      (awesome-pair-kill)
      (yank)
      (goto-char pt)))
  (remove-hook 'awesome-pair-mode-map 'SAS-mode))

;; -------- use thing-edit to cut/copy quickly --------
(use-package thing-edit
  :load-path "lazycat-bucket/thing-edit"
  :if (file-exists-p
       (expand-file-name "lazycat-bucket/thing-edit/thing-edit.el"
                         user-emacs-directory))
  :defer t
  :config
  (defun con5-thing-cut-region-or-symbol ()
    "Cut content of the current region or symbol around cursor."
    (interactive)
    (if (region-active-p)
        (thing-cut-region-or-line)
      (thing-cut-symbol)))
  (defun con5-thing-copy-region-or-symbol ()
    "Copy content of the current region or symbol around cursor."
    (interactive)
    (if (region-active-p)
        (thing-copy-region-or-line)
      (thing-edit 'symbol)))
  :bind
  (("C-M-w" . thing-copy-whole-buffer)
   ("C-S-y" . thing-replace-symbol)
   ("C-w" . con5-thing-cut-region-or-symbol)
   ("M-w" . con5-thing-copy-region-or-symbol)))

;; use `expand-region' to mark region
(use-package expand-region
  :bind
  (("M-'" . er/mark-inside-pairs)
   ;; ("M-]" . er/expand-region)
   ;; ("M-[" . er/contract-region)
   ))

;; -------- undo-tree --------
(use-package undo-tree
  :defer t
  :diminish undo-tree-mode
  :hook
  (after-init . global-undo-tree-mode)
  :config
  (setq undo-tree-visualizer-diff t
        undo-tree-auto-save-history nil
        undo-tree-enable-undo-in-region nil
        ;; Increase undo-limits by a factor of ten to avoid emacs prematurely
        ;; truncating the undo history and corrupting the tree. See
        ;; https://github.com/syl20bnr/spacemacs/issues/12110
        undo-limit 800000
        undo-strong-limit 12000000
        undo-outer-limit 120000000)
  ;; ;; when using dvorak layout
  ;; (global-set-key (kbd "C-z") 'undo-tree-undo)
  ;; (global-set-key (kbd "C-S-z") 'undo-tree-redo)
  :bind
  (:map undo-tree-map
        ("M-_" . nil)))

(provide '+editing)

;;; +editing.el ends here
