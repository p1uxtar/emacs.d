;;; +vcs.el --- vcs configurations

;;; Commentary:

;;; Code:

;; -------- highlight uncommitted changes --------
(use-package diff-hl
  :defer t
  :diminish diff-hl-mode
  :if (executable-find "git")
  :hook
  ((after-init . global-diff-hl-mode)
   (dired-mode . diff-hl-dired-mode))
  :config
  (diff-hl-margin-mode)
  (setq diff-hl-side 'right))

;; -------- (ma)git --------
(use-package magit
  :defer t
  :if (executable-find "git")
  :mode (("\\COMMIT_EDITMSG\\'" . text-mode)
         ("\\MERGE_MSG\\'" . text-mode))
  :init
  (setq magit-commit-ask-to-stage nil
        magit-diff-refine-hunk t)
  :config
  (setq magit-auto-revert-mode nil)
  :bind
  (("C-x g" . magit-status)))

(provide '+vcs)

;;; +vcs.el ends here
