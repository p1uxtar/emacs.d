;;; +msWin.el --- config for MS Windows -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- config for sys Win --------

(when (eq system-type 'windows-nt)
  ;; set default directory
  (setq default-directory "d:/")

  ;; left Win-key as super
  (setq w32-pass-lwindow-to-system nil)
  (setq w32-lwindow-modifier 'super) ;; will disable Win-* shortcuts
  ;; ;; right Win-key as super
  ;; (setq w32-pass-rwindow-to-system nil)
  ;; (setq w32-rwindow-modifier 'super)
  (w32-register-hot-key [s-])

  ;; menu/App-key as hyper
  (setq w32-pass-apps-to-system nil)
  (setq w32-apps-modifier 'hyper)
  (w32-register-hot-key [H-]))


;; -------- auto-hot-key --------
(when (eq system-type 'windows-nt)
  (use-package ahk-mode
    :defer t))

;; ;; via: emacs-china.org/t/topic/14231/7 @dingyi342
;; ;; set coding config, last is highest priority.
;; ;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Recognize-Coding.html#Recognize-Coding
;; (prefer-coding-system 'cp950)
;; (prefer-coding-system 'gb2312)
;; (prefer-coding-system 'cp936)
;; (prefer-coding-system 'gb18030)
;; (prefer-coding-system 'utf-16)
;; (prefer-coding-system 'utf-8-dos)
;; ;; (prefer-coding-system 'utf-8-unix)
;; (prefer-coding-system 'utf-8)

;; ;; via: emacs-china.org/t/win-modeline/13214/11 @zhscn
;; (when (eq system-type 'windows-nt)
;;   ;; (setq abbreviated-home-dir "\\`'")
;;   (setq locale-coding-system 'gb18030)
;;   (setq w32-unicode-filenames 'nil)
;;   (setq file-name-coding-system 'gb18030))

;; ;; -------- global utf-8 on Win --------
;; (when (eq system-type 'windows-nt)
;;   (modify-coding-system-alist 'process "*color-rg*" 'chinese-gb18030)
;;   (modify-coding-system-alist 'process "*eshell*" 'chinese-gb18030)
;;   (modify-coding-system-alist 'file "\\.md\\'\\|\\.org\\'" 'utf-8)
;;   (when (eq w32-ansi-code-page 65001)
;;     (setq w32-system-coding-system 'utf-8)
;;     (define-coding-system-alias 'cp65001 'utf-8)))

(provide '+msWin)

;;; +msWin.el ends here
