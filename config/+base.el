;;; +base.el --- basic configs -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- faster init via centaur.emacs --------
;; Package initialize occurs automatically, before `user-init-file' is
;; loaded, but after `early-init-file'. We handle package
;; initialization, so we must prevent Emacs from doing it early!
(add-hook 'before-init-hook
          (lambda ()
            ;; Inhibit resizing frame
            (setq frame-inhibit-implied-resize t)
            ;; Not startup package
            (setq package-enable-at-startup nil)
            ;; Faster to disable these here (before they've been initialized)
            (add-to-list 'default-frame-alist '(menu-bar-lines . 0))
            (add-to-list 'default-frame-alist '(tool-bar-lines . 0))
            (add-to-list 'default-frame-alist '(scroll-bar-mode . 0))
            (add-to-list 'default-frame-alist '(horizontal-scroll-bar-mode . 0))
            (when (featurep 'ns)
              (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t)))
            ))

;; -------- after-init-hook --------
(add-hook 'after-init-hook
          (lambda ()
            ;; 不修飾frame-title
            ;; (set-frame-parameter (car (frame-list)) 'undecorated t)
            (setq default-frame-alist '((undecorated . t)))
            ;; 鼠標拖動可邊界
            ;; (add-to-list 'default-frame-alist '(drag-internal-border . 1))
            (add-to-list 'default-frame-alist '(frame-internal-border-width . 1))
            (when (fboundp 'menu-bar-mode)
              (menu-bar-mode -1))
            (when (fboundp 'tool-bar-mode)
              (tool-bar-mode -1))
            ;; 水平滾動條
            (when (fboundp 'scroll-bar-mode)
              (scroll-bar-mode -1))
            ;; 垂直滾動條
            (when (fboundp 'horizontal-scroll-bar-mode)
              (horizontal-scroll-bar-mode -1))
            ;; scratch buffer mode
            ;; (setq initial-major-mode 'lisp-interaction-mode)
            (setq inhibit-startup-message t)
            (setq initial-scratch-message
                  ";; 道友留步，\n;; Enjoy counting the parentheses!\n\n")
            (setq inhibit-splash-screen t
                  inhibit-startup-screen t
                  inhibit-startup-echo-area-message t)))

;; -------- emacs-startup-hook --------
(add-hook 'emacs-startup-hook
          (lambda ()
            ;; garbage-collection
            (setq gc-cons-threshold (max most-positive-fixnum (* 1024 1024 1024)))
            ;; when idle for 10 seconds or when Emacs is unfocused.
            (run-with-idle-timer 10 t #'garbage-collect)
            ;; Don’t compact font caches during GC.
            (setq inhibit-compacting-font-caches t)
            ;; display init time
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)
            (setq file-name-handler-alist nil)))

;; -------- window-setup-hook --------
(add-hook 'window-setup-hook
          (lambda ()
            ;; cursor never blink
            (blink-cursor-mode -1)
            ;; highlight current line
            (global-hl-line-mode 1)
            ;; auto reload file
            (global-auto-revert-mode 1)
            (diminish 'auto-revert-mode)
            ;; save place in file
            (save-place-mode 1)))

;; -------- disable bell sound --------
(setq ring-bell-function 'ignore)

;; -------- no file.bk~ --------
(setq make-backup-files nil)

;; -------- sentence end and Not with double space --------
(setq sentence-end "\\([。；！？]\\|……\\|[.?!][]\"')}]*\\($\\|[ \t]\\)\\)[ \t\n]*")
(setq sentence-end-double-space nil)

;; -------- Tab and Space --------
;; Permanently indent with spaces, never with TABs
(setq-default c-basic-offset     4
              default-tab-width  4
              tab-width          4
              indent-tabs-mode nil)

;; -------- coding-system --------
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)

;; -------- case sensitive replace --------
(defadvice replace-string
    (around turn-off-case-fold-search)
  "Replace string with case sensitive."
  (let ((case-fold-search nil))
    ad-do-it))
(ad-activate 'replace-string)

;; -------- kill line including '\n' --------
;; (setq kill-whole-line t)

;; -------- whether add newline at bottom --------
;; (setq-default require-final-newline nil)

;; -------- show ediff in plain & be horizontal --------
(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-horizontally)

;; -------- always use `y-or-n-p' --------
(defalias 'yes-or-no-p 'y-or-n-p)
;; (fset 'yes-or-no-p 'y-or-n-p)

;; -------- disable left mouse button click paste --------
(setq mouse-yank-at-point t)
;; disable middle mouse button click
(global-unset-key (kbd "<mouse-2>"))
;; disable the right mouse button and double click to cut
(fset 'mouse-save-then-kill 'ignore)

(provide '+base)

;;; +base.el ends here
