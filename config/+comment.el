;;; +comment.el --- comment editing using separedit -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- Edit comment/string/docstring/code block --------
;; in separate buffer with specific mode.
(use-package separedit
  :defer t
  :hook
  ((prog-mode) . separedit-mode)
  :config
  ;; Key binding for modes you want edit
  ;; or simply bind ‘global-map’ for all.
  (define-key prog-mode-map        (kbd "C-c '") #'separedit)
  (define-key minibuffer-local-map (kbd "C-c '") #'separedit)
  (define-key help-mode-map        (kbd "C-c '") #'separedit)
  ;; Default major-mode for edit buffer
  ;; can also be other mode e.g. ‘org-mode’.
  (setq separedit-default-mode 'markdown-mode)
  ;; Feature options
  (setq separedit-preserve-string-indentation t)
  ;; (setq separedit-continue-fill-column t)
  ;; (setq separedit-write-file-when-execute-save t)
  ;; (setq separedit-remove-trailing-spaces-in-comment t)
  )

(provide '+comment)

;;; +comment.el ends here
