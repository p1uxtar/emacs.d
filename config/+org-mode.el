;;; +org-mode.el --- org-mode -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- org-mode --------
(setq org-startup-indented t)
(setq-default org-display-custom-times t)
;; (setq org-time-stamp-custom-formats '("<%a %b %e %Y>" . "<%a %b %e %Y %H:%M>"))
(setq org-time-stamp-custom-formats '("<%Y-%m-%d, %w>" . "<%a %b %e %Y %H:%M>"))

(add-hook 'org-mode-hook
          (lambda ()
            (diminish 'org-indent-mode)
            ;; use inline format for CN, via: emacs-china.org/t/orgmode/9740/17
            (setq org-emphasis-regexp-components '("-[:multibyte:][:space:]('\"{" "-[:multibyte:][:space:].,:!?;'\")}\\[" "[:space:]" "." 1))
            (org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components)
            (org-element-update-syntax)
            ;; hide emphasis marker characters */_=~+ etc.
            (setq org-hide-emphasis-markers t)
            ;; visual alignment
            (valign-mode)
            ;; sub super script be added {}
            (setq org-use-sub-superscripts "{}")
            (setq org-export-with-sub-superscripts '{})
            ;; highlight sub super script
            (setq org-highlight-latex-and-related '(native script entities)
                  org-pretty-entities t
                  org-pretty-entities-include-sub-superscripts t)))

;; (add-hook 'org-mode-hook
;;           (lambda ()
;;             ;; (require 'org-tempo)
;;             (local-set-key (kbd "C-c o s") #'p1uxtar-org-insert-src)))

;; org-babel
(with-eval-after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages '((C . t)
                               (emacs-lisp . t)
                               (latex . t)
                               (python . t)
                               (R . t)
                               (shell . t)))
  (setq org-confirm-babel-evaluate nil))

(provide '+org-mode)

;;; +org-mode.el ends here
