;;; +color-rg.el --- config for searching by color-rg -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; -------- color-rg --------
(use-package color-rg
  :load-path "lazycat-bucket/color-rg"
  :if (file-exists-p
       (expand-file-name "lazycat-bucket/color-rg/color-rg.el"
                         user-emacs-directory))
  :config
  (defhydra hydra-color-rg ()
    ("M-b" backward-word "前一詞" :column "移動")
    ("M-f" forward-word "後一詞")
    ("C-n" next-line "下一行")
    ("C-p" previous-line "上一行")
    ("C-v" scroll-up-command "翻頁")
    ("M-v" scroll-down-command "向上翻頁")
    ("f" color-rg-search-input-in-current-file "輸入內容（文件）" :column "搜索")
    ("c" color-rg-search-symbol-in-current-file "當前符號（文件）")
    ("i" color-rg-search-input-in-project "輸入內容（項目）")
    ("s" color-rg-search-symbol-in-project "當前符號（項目）")
    ("N" color-rg-rerun-toggle-node "無視忽略條件")
    ("q" nil "退出")
    ("n" color-rg-jump-next-keyword "下一匹配" :column "結果跳轉")
    ("p" color-rg-jump-prev-keyword "上一匹配")
    ("r" color-rg-replace-all-matches "替換")
    ("M-n" color-rg-jump-next-file "下一文件")
    ("M-p" color-rg-jump-prev-file "上一文件")
    ("C-o" other-window "源文件"))
  (setq color-rg-search-ignore-rules
        (concat color-rg-search-ignore-rules
                " -g '!elpa*' -g '!lazycat-bucket*' -g '!site-lisp*'"))
  :bind
  (("C-M-s" . 'hydra-color-rg/body)))

(provide '+color-rg)

;;; +color-rg.el ends here
