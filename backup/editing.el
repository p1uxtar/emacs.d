;; -------- Eclipse-like moving and duplicating lines or rectangles --------
(use-package move-dup
  :defer t
  :bind
  (("<M-up>"   . move-dup-move-lines-up)
   ("C-M-p"    . move-dup-duplicate-up)
   ("<M-down>" . move-dup-move-lines-down)
   ("C-M-n"    . move-dup-duplicate-down)))

;; -------- map functions when selected (`region-active-p') --------
;; github.com/Kungsgeten/selected.el
(use-package selected
  :after thing-edit
  :diminish selected-minor-mode
  :commands selected-minor-mode
  :init
  (selected-global-mode)
  :bind
  (:map selected-keymap
        (("C-w" . thing-cut-region-or-line)
         ("M-w" . thing-copy-region-or-line))))

;; -------- kill word or region backward --------
(defun p1uxtar-backward-kill-word-or-region (&optional arg)
  (interactive "p")
  (if (region-active-p)
      (call-interactively #'kill-region)
    (backward-kill-word arg)))
;; (global-set-key (kbd "C-w")  'p1uxtar-backward-kill-word-or-region)
;; replace with LazyCat/thing-edit

;; Resolve diff3 conflicts
(use-package smerge-mode
  :defer t
  :diminish
  :bind (:map smerge-mode-map
              ("C-c m" . smerge-mode-hydra/body))
  :hook ((find-file . (lambda ()
                        (save-excursion
                          (goto-char (point-min))
                          (when (re-search-forward "^<<<<<<< " nil t)
                            (smerge-mode 1)))))
         (magit-diff-visit-file . (lambda ()
                                    (when smerge-mode
                                      (smerge-mode-hydra/body))))))
