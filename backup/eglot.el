;; -------- C & C++ --------
;; config language server using ccls or clangd
(use-package eglot
  :defer t
  :config
  (let* ((ccp (or (executable-find "ccls")
                  (executable-find "clangd"))))
    (when ccp
      (add-to-list 'eglot-server-programs
                   '((cc-mode) . (ccp)))
      (eglot-ensure))))

;; -------- Python --------
(when (executable-find "pyls")
  (cond
   ((eq system-type 'gnu/linux)
    (add-to-list 'eglot-server-programs
                 `(python-mode . ("pyls" "-v" "--tcp" "--host"
	                              "localhost" "--port" :autoport))))
   ((eq system-type 'windows-nt)
    (pyvenv-activate "c:/Users/Yuanchen.Xie/Anaconda3/python.exe")
    (add-to-list 'eglot-server-programs
                 `(python-mode . ("c:/Users/Yuanchen.Xie/Anaconda3/Scripts/pyls.exe" "-v" "--tcp" "--host"
	                              "localhost" "--port" :autoport)))))
  (eglot-ensure))

;; -------- R --------
(use-package eglot
  :defer t
  :config
  (add-to-list 'eglot-server-programs
               '((ess-r-mode inferior-ess-r-mode)
                 . ("R" "--slave" "-e" "languageserver::run()"))))

;; -------- LaTeX --------
;; either $ yay -S luarocks # lua
;;   then $ luarocks --server http://luarocks.org/dev install digestif --local
;;        $ luarocks install digestif --local
;; or $ yay -S texlab
;; or $ cargo install --locked --git https://github.com/latex-lsp/texlab.git
;; to use "~/.luarocks/bin/digestif" or "/usr/bin/texlab"
(use-package eglot
  :defer t
  :config
  (add-hook 'LaTeX-mode
            (lambda ()
              (let ((texlsp
                     (executable-find "digestif")
                     ;; (executable-find "~/.cargo/bin/texlab")
                     ))
                (when texlsp
                  (add-to-list 'eglot-server-programs
		                       '((tex-mode context-mode texinfo-mode bibtex-mode)
		                         . (texlsp)))
                  (eglot-ensure))))))
