;; -------- dumb-jump --------
(use-package dumb-jump
  :config
  (dumb-jump-mode)
  (setq dumb-jump-selector 'ivy)
  :bind
  (("M-g o" . dumb-jump-go-other-window)
   ("M-g j" . dumb-jump-go)
   ("M-g b" . dumb-jump-back)
   ("M-g i" . dumb-jump-go-prompt)
   ("M-g x" . dumb-jump-go-prefer-external)
   ("M-g z" . dumb-jump-go-prefer-external-other-window)))

;; -------- ido --------
;; switch between buffers and visit files and directories
;; with a minimum of keystrokes.
(use-package ido
  :config
  (setq ido-create-new-buffer 'always)
  (setq ido-enable-flex-matching t)
  (setq ido-enable-last-directory-history nil)
  (setq ido-everywhere t)
  (setq ido-use-filename-at-point 'guess)
  (setq ido-use-virtual-buffers t)
  (ido-mode 1))

;; enhance M-x. Alternative M-x with extra features.
(use-package amx
  :init
  (setq amx-history-length 20)
  :hook
  ((ivy-mode) . amx-mode))

;; -------- projectile --------
(use-package projectile
  :defer t
  :diminish projectile-mode
  :init
  (setq projectile-completion-system 'ivy)
  :config
  (projectile-mode)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))

;; ivy integration for projectile
(use-package counsel-projectile
  :defer t
  :config
  (counsel-projectile-mode 1))

;; relative config
(use-package ivy-posframe
  :custom
  (ivy-posframe-height-alist '((projectile-find-file         . 20)
                               (counsel-projectile-find-file . 20))))

;; -------- swiper --------
(use-package swiper
  :defer t
  :bind
  (("C-s" . 'swiper-isearch)))

;; relative config
(use-package counsel
  :bind
  (:map counsel-mode-map
        ([remap swiper] . counsel-grep-or-swiper)))

(use-package ivy-prescient
  :init
  (setq ivy-prescient-retain-classic-highlighting t
	    ivy-re-builders-alist
	    '((swiper . ivy-prescient-non-fuzzy)
	      (swiper-isearch . ivy-prescient-non-fuzzy)
	      (swiper-all . ivy-prescient-non-fuzzy)
	      (t . ivy-prescient-re-builder))
	    ivy-prescient-sort-commands
	    '(:not swiper swiper-isearch)))

(use-package ivy-posframe
  :custom
  (ivy-posframe-height-alist
   '((swiper-isearch . 20))))

;; -------- Display available keybindings --------
(use-package which-key
  :diminish which-key-mode
  :custom
  (which-key-idle-delay 0.3)
  (which-key-popup-type 'side-window)
  (which-key-side-window-location 'bottom)
  (which-key-show-docstrings t)
  ;; (which-key-max-display-columns 1)
  (which-key-show-prefix 'left)
  ;; (which-key-side-window-max-height 10)
  ;; (which-key-max-description-length 30)
  :config
  (which-key-mode t))
