;;; +lazy-load.el --- config -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(use-package lazy-load
  :load-path "lazycat-bucket/lazy-load"
  :if (file-exists-p
       (expand-file-name "lazycat-bucket/lazy-load/lazy-load.el"
                         user-emacs-directory)))

(with-eval-after-load 'lazy-load
  ;; magit
  (progn
    (lazy-load-unset-keys '("C-x g"))
    (lazy-load-global-keys
     '(("C-x g" . magit-status))
     (concat user-emacs-directory (car (s-match "elpa/magit-[0-9.]*" (prin1-to-string load-path))) "/magit")))
  ;; ivy-yasnippet
  (lazy-load-global-keys
   '(("C-c y" . ivy-yasnippet))
   (concat user-emacs-directory (car (s-match "elpa/ivy-yasnippet-[0-9.]*" (prin1-to-string load-path))) "/ivy-yasnippet")))

(provide '+lazy-load)

;;; +lazy-load.el ends here
