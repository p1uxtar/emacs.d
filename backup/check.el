;; -------- syntax checking --------
(use-package flycheck
  :defer t
  :diminish flycheck-mode
  :hook
  (((after-init) . flycheck-mode)
   ((prog-mode) . flycheck-mode))
  :config
  (global-flycheck-mode -1)
  (dolist (hook (list
                 'inferior-ess-r-mode
                 'ess-r-mode
                 'SAS-mode
                 ))
    (add-hook hook (lambda ()
                     (flycheck-mode -1))))
  (use-package flycheck-posframe
    :if (display-graphic-p)
    :custom
    (flycheck-posframe-position 'window-bottom-left-corner)
    (flycheck-posframe-face ((t (:inherit font-lock-warning-face :bold t :height 2.0 :background "DarkRed"))))
    :hook
    ((flycheck-mode) . flycheck-posframe-mode)))
