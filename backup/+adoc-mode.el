;;; Code:

;; -------- AsciiDoc --------
(use-package adoc-mode
  :mode
  (("*\\.asc\\'" . adoc-mode))
  ;; :init
  ;; this will warning `Package cl is deprecated' after Emacs v27
  ;; use code below to get which dependent cl:
  ;; (require 'loadhist)
  ;; (file-dependents (feature-file 'cl))
  )

(provide '+adoc-mode)

;;; +adoc-mode.el ends here
