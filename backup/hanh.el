;;; Code:

;; -------- input method --------

;; -------- fcitx --------
(when (executable-find "fcitx")
  (use-package fcitx
    :defer t
    :config
    (fcitx-aggressive-setup)
    (fcitx-prefix-keys-add "<f1>")
    (setq fcitx-use-dbus t)))

;; -------- pyim --------
(when (and (eq system-type 'windows-nt)
           (display-graphic-p))
  (add-hook
   'window-setup-hook
   (lambda ()
     (use-package pyim-cangjiedict
       :defer t
       :init
       (setq pyim-default-scheme 'cangjie)
       (pyim-cangjie5dict-enable))
     ;; use pyim after adding *dict.pyim
     (use-package pyim
       :after pyim-cangjiedict
       :demand t
       :diminish pyim
       :custom
       (pyim-punctuation-dict
        '(("'" "'")
          ("\"" "\"")
          ("_" "_")
          ("^" "^")
          ("]" "]")
          ("[" "[")
          ("@" "@")
          ("?" "？")
          (">" ">")
          ("=" "=")
          ("<" "《》")
          (";" "；")
          (":" "：")
          ("/" "、")
          ("." "。")
          ("-" "-")
          ("," "，")
          ("+" "+")
          ("*" "*")
          (")" ")")
          ("(" "(")
          ("&" "&")
          ("%" "%")
          ("$" "$")
          ("#" "#")
          ("!" "！")
          ("`" "`")
          ("~" "~")
          ("}" "』")
          ("|" "|")
          ("{" "『")))
       :config
       (setq default-input-method "pyim"
             pyim-enable-shortcode nil
             pyim-punctuation-translate-p '(auto yes no))
       (setq pyim-page-tooltip 'popup)
       (cond
        ((string= *theme-type* "hesperus")
         (setq pyim-indicator-cursor-color (list "#e67e22" "gold")))
        ((string= *theme-type* "phosphorus")
         (setq pyim-indicator-cursor-color (list "#8d4bbb" "#4078f2"))))
       (setq-default pyim-english-input-switch-functions
                     '(pyim-probe-program-mode
                       pyim-probe-org-structure-template)
                     pyim-punctuation-half-width-functions
                     '(pyim-probe-punctuation-line-beginning
                       pyim-probe-punctuation-after-punctuation))
       (define-key pyim-mode-map "-"
                   (lambda ()
                     (interactive)
                     (pyim-select-word-by-number 2)))
       :bind
       (("C-j" . pyim-convert-string-at-point)
        ;; ("C-;" . pyim-delete-word-from-personal-buffer)
        )
       (:map pyim-mode-map
             ("." . pyim-page-next-page)
             ("," . pyim-page-previous-page))))))

;; -------- rime --------
(when (and (string-equal system-type "gnu/linux")
           (display-graphic-p))
  (use-package rime
    :custom
    (default-input-method "rime")
    (rime-disable-predicates '(rime-predicate-prog-in-code-p ;; disable when coding
			                   rime-predicate-after-alphabet-char-p)) ;; disable after alphabet
    (rime-inline-predicates '(rime-predicate-space-after-cc-p ;; cc after space
			                  rime-predicate-current-uppercase-letter-p)) ;; typing in uppercase
    :config
    (setq rime-cursor "°"
	      rime-title "RIME"
	      rime-translate-keybindings
	      '("C-f" "C-b" "C-n" "C-p" "C-g" "<left>" "<right>" "<up>" "<down>" "<prior>" "<next>" "<delete>" "C-k" "C-v" "M-v")
          rime-share-data-dir "~/.config/fcitx/rime"
	      rime-user-data-dir "~/.emacs.d/rime")
    ;; reference mode-line-mule-info value, there may be other useful information
    (setq mode-line-mule-info '((:eval (rime-lighter))))
    ;; (with-eval-after-load 'rime
    ;;   (use-package cursor-chg
    ;;     :load-path "site-lisp"
    ;;     :init
    ;;     (custom-set-variables
    ;;      '(curchg-default-cursor-type 'box))
    ;;     :config
    ;;     (toggle-cursor-type-when-idle 1) ;; Turn on cursor change when Emacs is idle
    ;;     (change-cursor-mode 1) ;; Turn on change for overwrite, read-only, and input mode
    ;;     ))
    ;; (if (display-graphic-p)
	(setq rime-show-candidate 'posframe
	      ;; rime-posframe-properties
	      ;; (list :background-color "#34495e"
		  ;;       :foreground-color "#3de1ad"
		  ;;       ;; :font "WenQuanYi Micro Hei Mono-14"
		  ;;       :font "Source Han Sans CN"
		  ;;       :internal-border-width 10)
	      rime-posframe-style 'vertical)
    ;; (setq rime-show-candidate 'minibuffer))
    (define-key rime-mode-map (kbd "M-j") 'rime-force-enable)
    :bind
    (:map rime-mode-map
	      ("M-j" . #'rime-inline-ascii)
	      ("C-`" . #'rime-send-keybinding))))

;; -------- translation --------

;; -------- G translate --------
;; google-translate
(use-package google-translate-posframe
  :load-path "site-lisp"
  :bind
  (("C-c t" . 'google-translate-chinese-at-point++)))

;; lorniu/go-translate
(use-package go-translate
  :defer t
  :commands (go-translate go-translate-popup)
  :init
  (setq go-translate-base-url "https://translate.google.cn")
  (setq go-translate-local-language "zh-CN")
  (setq go-translate-extra-directions '(("zh-CN" . "en")
                                        ;; ("en" . "fr")
                                        ))
  (setq go-translate-buffer-follow-p t)
  (setq go-translate-inputs-function
        #'go-translate-inputs-current-or-prompt)
  ;; (defun go-translate-token--extract-tkk ()
  ;;   (cons 430675 2721866130))
  :bind
  (("C-c t" . go-translate-popup-current)
   ("C-c T" . go-translate-popup)))

;; -------- insert-translated-name --------
(use-package insert-translated-name
  :load-path "lazycat-bucket/insert-translated-name"
  :bind
  (("C-c I" . 'insert-translated-name-insert)))

;; -------- stardict by pRot0ta1p --------
(use-package stardict
  :load-path "site-lisp/pRot0ta1p-stardict"
  :defer t
  :config
  (stardict-add-dictionary :lang "Eng"
                           :path "~/.emacs.d/dicts/stardict-oxford-gb-2.4.2/"
                           :filename "oxford-gb"
                           :persist t)
  :bind
  (("C-c S" . 'stardict-translate-minibuffer)))

;; -------- define youdao to display with posframe --------
(with-eval-after-load 'posframe
  (defun youdao-dictionary-search-at-point-posframe ()
	"Search word at point and display result with posframe."
	(interactive)
	(let ((word (youdao-dictionary--region-or-word)))
	  (if word
		  (progn
		    (with-current-buffer (get-buffer-create youdao-dictionary-buffer-name)
		      (let ((inhibit-read-only t))
		        (erase-buffer)
		        (youdao-dictionary-mode)
		        (insert (youdao-dictionary--format-result word))
		        (goto-char (point-min))
		        (set (make-local-variable 'youdao-dictionary-current-buffer-word) word)))
		    (posframe-show youdao-dictionary-buffer-name
				           :internal-border-width 5
				           :position (point))
		    (unwind-protect
		        (push (read-event) unread-command-events)
		      (posframe-hide youdao-dictionary-buffer-name)))
	    (message "Nothing to look up")))))

;;; hanh.el ends here
