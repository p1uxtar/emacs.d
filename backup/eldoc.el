;; -------- eldoc for eglot --------
(use-package eldoc-box
  :defer t
  :if window-system
  :diminish (eldoc-mode eldoc-box-hover-at-point-mode)
  :hook
  ((eglot--managed-mode . eldoc-box-hover-at-point-mode)
   (eldoc-mode . eldoc-box-hover-at-point-mode)))
