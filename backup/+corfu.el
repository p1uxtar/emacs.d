;;; +corfu.el --- completion overlay -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(use-package corfu
  :init
  (global-corfu-mode)
  :config
  ;; Enable auto completion and configure quitting
  (setq corfu-cycle t ;; Enable cycling for `corfu-next/previous'
        corfu-auto t ;; Enable auto completion
        corfu-max-width 110
        corfu-auto-delay 0.2
        corfu-auto-prefix 1
        ;; (corfu-quit-at-boundary t) ;; Automatically quit at word boundary
        corfu-quit-no-match 'separator ;; Automatically quit if there is no match
        corfu-preview-current nil    ;; Disable current candidate preview
        corfu-echo-documentation t ;; echo documentation in the echo area
        )
  (defun con5-yas-or-corfu-complete ()
    (interactive)
    (or (yas-expand)
        (corfu-insert)))
  :bind
  (:map corfu-map
        ("<tab>" . corfu-next)
        ;; ("<tab>" . con5-yas-or-corfu-complete)
        ("<backtab>" . corfu-previous)
        ("<escape>" . corfu-quit)))

(use-package orderless
  :demand t
  :config
  (setq completion-styles '(orderless flex)
        completion-category-defaults nil
        completion-category-overrides '((eglot (styles . (orderless flex))))))

;; Add extensions
(use-package cape
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  :bind (("C-c p p" . completion-at-point) ;; capf
         ("C-c p t" . complete-tag)        ;; etags
         ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
         ("C-c p h" . cape-history)
         ("C-c p f" . cape-file)
         ("C-c p k" . cape-keyword)
         ("C-c p s" . cape-elisp-symbol)
         ("C-c p e" . cape-elisp-block)
         ("C-c p a" . cape-abbrev)
         ("C-c p l" . cape-line)
         ("C-c p w" . cape-dict)
         ("C-c p :" . cape-emoji)
         ("C-c p \\" . cape-tex)
         ("C-c p _" . cape-tex)
         ("C-c p ^" . cape-tex)
         ("C-c p &" . cape-sgml)
         ("C-c p r" . cape-rfc1345))
  :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  (add-hook 'completion-at-point-functions #'cape-dabbrev)
  (add-hook 'completion-at-point-functions #'cape-file)
  (add-hook 'completion-at-point-functions #'cape-elisp-block)
  ;;(add-hook 'completion-at-point-functions #'cape-history)
  ;;(add-hook 'completion-at-point-functions #'cape-keyword)
  ;;(add-hook 'completion-at-point-functions #'cape-tex)
  ;;(add-hook 'completion-at-point-functions #'cape-sgml)
  ;;(add-hook 'completion-at-point-functions #'cape-rfc1345)
  ;;(add-hook 'completion-at-point-functions #'cape-abbrev)
  ;;(add-hook 'completion-at-point-functions #'cape-dict)
  ;;(add-hook 'completion-at-point-functions #'cape-elisp-symbol)
  ;;(add-hook 'completion-at-point-functions #'cape-line)
  )

;; -------- yasnippet --------
(use-package yasnippet
  :defer t
  :load-path "site-lisp/yasnippet"
  :diminish yas-minor-mode
  :init
  (use-package yasnippet-snippets
    :defer t
    :after yasnippet)
  :hook
  ((prog-mode markdown-mode LaTeX-mode org-mode text-mode ess-mode) . yas-minor-mode)
  :config
  ;; (add-hook 'emacs-startup-hook
  ;;           (lambda ()
  ;;             ;; global mode will delay init time
  ;;             (yas-global-mode 1)))
  (use-package ivy-yasnippet
    :defer t
    :bind
    (("C-c y" . 'ivy-yasnippet)))
  ;; load the snippet tables and then call `yas-minor-mode' from the hooks of major-modes where you want YASnippet enabled.
  (yas-reload-all))

(provide '+corfu)

;;; +corfu.el ends here
