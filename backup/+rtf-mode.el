;;; Code:

(use-package rtf-mode
  :load-path "site-lisp")

(provide '+rtf-mode)

;;; +rtf-mode.el ends here
