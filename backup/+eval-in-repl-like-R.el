
;; -------- run like ESS --------
(use-package eval-in-repl
  :defer t
  :init
  (setq
   ;; Not to jump after evaluating current line
   eir-jump-after-eval nil
   ;; just split the current script window into two
   eir-always-split-script-window t
   ;; Place REPL on which side of the script window when splitting.
   eir-repl-placement 'right))

;;; Python support
;; (require 'python) ; if not done elsewhere
(require 'eval-in-repl-python)
(add-hook 'python-mode-hook
          '(lambda ()
             (local-set-key (kbd "<C-return>") 'eir-eval-in-python)))

;; Shell support
(setq explicit-shell-file-name "/usr/bin/bash")
;; (setq explicit-shell-file-name "/usr/bin/zsh")
(require 'eval-in-repl-shell)
(add-hook 'sh-mode-hook
          '(lambda()
             (local-set-key (kbd "C-<return>") 'eir-eval-in-shell)))

(provide '+eval-in-repl-like-R)
