;;; +azerty-mode.el --- minor-mode -*- lexical-binding: t -*-

;; Author: Yuanchen Xie

;;; Commentary:

;; remap keybindings to adapt the azerty (French) layout.

;;; Code:

(define-minor-mode azerty-mode
  "Remap keybindings to adapt the azerty (French) layout."
  :group azerty
  :lighter " Â"
  :keymap (let ((map (make-sparse-keymap)))
            ;; (define-key map (kbd "C-:") 'repeat)
            ;; (define-key map (kbd "C-a") 'quoted-insert)
            ;; (define-key map (kbd "C-q") 'mwim-beginning-of-code-or-line)
            ;; (define-key map (kbd "M-a") 'fill-paragraph)
            ;; (define-key map (kbd "M-q") 'backward-sentence)
            (define-key map (kbd "s-SPC") 'toggle-input-method)
            (define-key map (kbd "C-M-²") 'beginning-of-buffer)
            (define-key map (kbd "C-M-ù") 'end-of-buffer)
            ;; (define-key map (kbd "C-x &") 'delete-other-windows)
            ;; (define-key map (kbd "C-x é") 'p1uxtar-split-window-below)
            ;; (define-key map (kbd "C-x \"") 'p1uxtar-split-window-right)
            ;; (define-key map (kbd "C-x à") 'delete-window)
            map)
  (define-key key-translation-map (kbd "C-²") (kbd "<"))
  (define-key key-translation-map (kbd "C-ù") (kbd ">"))
  (define-key key-translation-map (kbd "M-_") (kbd "\\"))
  ;; -------- multiple-cursors & expand-region --------
  (when (and (package-installed-p 'multiple-cursors)
             (package-installed-p 'hydra))
    (use-package multiple-cursors
      :config
      (custom-set-variables
       '(mc/insert-numbers-default 1))
      ;; -------- hydra --------
      (defhydra hydra-multiple-cursors (global-map "C-;" :hint nil)
        "
 Up^^             Down^^           Miscellaneous           % 2(mc/num-cursors) cursor%s(if (> (mc/num-cursors) 1) \"s\" \"\")
------------------------------------------------------------------
 [_p_]   Next     [_n_]   Next     [_l_] Edit lines  [_0_] Insert numbers
 [_P_]   Skip     [_N_]   Skip     [_a_] Mark all    [_A_] Insert letters
 [_U_]   Unmark   [_u_]   Unmark   [_r_] Search
                [_w_]   word     [_W_] All-Words
                [_s_]   symbol   [_S_] All-Symbols
 [Click] Cursor at point       [_q_] Quit        [_C-g_] Quit"
        ("l" mc/edit-lines :exit t)
        ("a" mc/mark-all-like-this :exit t)
        ("n" mc/mark-next-like-this)
        ("N" mc/skip-to-next-like-this)
        ("u" mc/unmark-next-like-this)
        ("w" mc/mark-next-like-this-word)
        ("s" mc/mark-next-like-this-symbol)
        ("p" mc/mark-previous-like-this)
        ("P" mc/skip-to-previous-like-this)
        ("U" mc/unmark-previous-like-this)
        ;; ("M-w" mc/mark-previous-like-this-word)
        ;; ("M-s" mc/mark-previous-like-this-symbol)
        ("r" mc/mark-all-in-region-regexp :exit t)
        ("W" mc/mark-all-words-like-this-in-defun)
        ("S" mc/mark-all-symbols-like-this-in-defun)
        ("0" mc/insert-numbers :exit t)
        ("A" mc/insert-letters :exit t)
        ("<mouse-1>" mc/add-cursor-on-click)
        ;; Help with click recognition in this hydra
        ("<down-mouse-1>" ignore)
        ("<drag-mouse-1>" ignore)
        ("q" nil)
        ("C-g" nil))))
  ;; (when (and (file-exists-p "~/.emacs.d/lazycat-bucket/thing-edit/thing-edit.el")
  ;;            (package-installed-p 'selected))
  ;;   ;; -------- use thing-edit to cut/copy quickly --------
  ;;   (use-package thing-edit
  ;;     :load-path "lazycat-bucket/thing-edit"
  ;;     :bind
  ;;     (:map azerty-mode-map
  ;;           ("C-M-z" . thing-copy-whole-buffer)
  ;;           ("C-S-y" . thing-replace-symbol)
  ;;           ("C-z" . thing-cut-symbol)
  ;;           ("M-z" . thing-copy-symbol)))
  ;;   ;; -------- map functions when selected (`region-active-p') --------
  ;;   (use-package selected
  ;;     :after thing-edit
  ;;     :diminish selected-minor-mode
  ;;     :commands selected-minor-mode
  ;;     :init
  ;;     (selected-global-mode)
  ;;     :bind
  ;;     (:map selected-keymap
  ;;           (("C-z" . thing-cut-region-or-line)
  ;;            ("M-z" . thing-copy-region-or-line)))))
  )

(add-hook 'emacs-startup-hook 'azerty-global-mode)
;; this hook effect after `after-init-hook'

(define-global-minor-mode azerty-global-mode
  azerty-mode
  turn-on-azerty-mode
  :group 'azerty
  :require 'azerty)

(defun turn-on-azerty-mode ()
  "Turn on azerty minor mode."
  (interactive)
  (azerty-mode +1))

(defun turn-off-azerty-mode ()
  "Turn off azerty minor mode."
  (interactive)
  (azerty-mode -1))

(provide '+azerty-mode)

;;; +azerty-mode.el ends here
