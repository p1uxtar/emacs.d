;; (defun p1uxtar-copy-kf-2-ch ()
;;   "Write current buffer to neighboring `CH' directory by one-key."
;;   (interactive)
;;   (eshell-command "rm ../CH/sed*")
;;   (let* ((current-buffer (concat (file-name-base (buffer-name))  ".sas")))
;;     (let* ((purpose-file (concat "../CH/" (replace-regexp-in-string "\s" "" current-buffer))))
;;       (write-file purpose-file)
;;       (eshell-command (concat "sed -i \"s/kf/ch/g\" " purpose-file)))
;;     (find-file (concat "../KF/" current-buffer))))

(defun p1uxtar-generate-sequence-strings (len &optional pre suf)
  "Generate strings with serial numbers between `PRE' and `SUF' from 1 to LEN."
  (interactive "nStrings from 1 to: \nsPrefix: \nsSuffix: ")
  (dotimes (x len)
    (insert (format (concat " " pre "%2.2d" suf)
                    (cl-incf x))))
  (message "Strings like \"%s ... %s\" had been generated %d times."
           pre suf len))

(define-minor-mode cdisc-mode
  "Assist in SAS programming for Clinical Data Interchange Standards."
  ;; :lighter ""
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "<f9>") 'p1uxtar-copy-kf-2-ch)
            map))

(add-hook 'SAS-mode-hook 'cdisc-mode)

(provide '+cdisc-mode)
